package cn.icanci.loopstack.rec.common.model.config;

import cn.icanci.loopstack.rec.common.enums.HttpRequestTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;
import cn.icanci.loopstack.rec.common.enums.DataSourceTypeEnum;

/**
 * 数据源
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:05
 */
public class DataSourceVO extends BaseVO {
    /**
     * 域Code
     */
    private String             domainCode;
    /**
     * 数据源名称
     */
    private String             dataSourceName;
    /**
     * 数据源类型
     */
    private DataSourceTypeEnum dataSourceType;
    /**
     * 数据源为脚本的执行数据集
     */
    private ScriptInfo         scriptInfo;
    /**
     * 数据源为HTTP的执行数据集
     */
    private HttpInfo           httpInfo;
    /**
     * 数据源为SQL的执行数据集
     */
    private SqlInfo            sqlInfo;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    public DataSourceTypeEnum getDataSourceType() {
        return dataSourceType;
    }

    public void setDataSourceType(DataSourceTypeEnum dataSourceType) {
        this.dataSourceType = dataSourceType;
    }

    public ScriptInfo getScriptInfo() {
        return scriptInfo;
    }

    public void setScriptInfo(ScriptInfo scriptInfo) {
        this.scriptInfo = scriptInfo;
    }

    public HttpInfo getHttpInfo() {
        return httpInfo;
    }

    public void setHttpInfo(HttpInfo httpInfo) {
        this.httpInfo = httpInfo;
    }

    public SqlInfo getSqlInfo() {
        return sqlInfo;
    }

    public void setSqlInfo(SqlInfo sqlInfo) {
        this.sqlInfo = sqlInfo;
    }

    /**
     * 数据源为脚本的执行数据集
     */
    public static class ScriptInfo {
        /**
         * 脚本执行类型
         *
         */
        private ScriptTypeEnum scriptType;
        /**
         * 脚本内容
         */
        private String         scriptContent;

        /**
         * 执行超时时间，秒
         * 为0则不做限制
         */
        private int            timeout;

        public ScriptTypeEnum getScriptType() {
            return scriptType;
        }

        public void setScriptType(ScriptTypeEnum scriptType) {
            this.scriptType = scriptType;
        }

        public String getScriptContent() {
            return scriptContent;
        }

        public void setScriptContent(String scriptContent) {
            this.scriptContent = scriptContent;
        }

        public int getTimeout() {
            return timeout;
        }

        public void setTimeout(int timeout) {
            this.timeout = timeout;
        }
    }

    /**
     * 数据源为HTTP的执行数据集
     */
    public static class HttpInfo {
        /**
         * 请求数据类型
         */
        private HttpRequestTypeEnum httpRequestType;
        /**
         * 请求接口
         */
        private String              reqUrl;
        /**
         * 请求参数
         * 
         * @see HttpRequestTypeEnum#POST 时有效
         */
        private String              reqParam;

        /**
         * 请求超时时间，秒
         * 为0则不做限制，则最大超时时间为5s
         */
        private int                 timeout;

        public HttpRequestTypeEnum getHttpRequestType() {
            return httpRequestType;
        }

        public void setHttpRequestType(HttpRequestTypeEnum httpRequestType) {
            this.httpRequestType = httpRequestType;
        }

        public String getReqUrl() {
            return reqUrl;
        }

        public void setReqUrl(String reqUrl) {
            this.reqUrl = reqUrl;
        }

        public String getReqParam() {
            return reqParam;
        }

        public void setReqParam(String reqParam) {
            this.reqParam = reqParam;
        }

        public int getTimeout() {
            return timeout;
        }

        public void setTimeout(int timeout) {
            this.timeout = timeout;
        }
    }

    /**
     * 数据源为SQL的执行数据集
     */
    public static class SqlInfo {
        /**
         * SQL 语句，只支持查询语句
         * 
         * 查询默认数据库 rec_ext
         */
        private String sql;

        /**
         * 执行超时时间，秒
         * 为0则不作限制
         */
        private int    timeout;

        public String getSql() {
            return sql;
        }

        public void setSql(String sql) {
            this.sql = sql;
        }

        public int getTimeout() {
            return timeout;
        }

        public void setTimeout(int timeout) {
            this.timeout = timeout;
        }
    }
}
