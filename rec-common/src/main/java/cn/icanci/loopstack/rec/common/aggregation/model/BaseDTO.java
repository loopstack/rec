package cn.icanci.loopstack.rec.common.aggregation.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 17:12
 */
public class BaseDTO implements Serializable {
    private static final long serialVersionUID = 1031085394922945504L;

    /**
     * 文档id
     */
    private String            id;

    /**
     * 雪花算法随机UUID
     */
    private String            uuid;

    /**
     * 功能描述
     */
    private String            desc;

    /**
     * 创建时间
     */
    private Date              createTime;

    /**
     * 更新时间
     */
    private Date              updateTime;

    /**
     * 状态 0有效，1无效
     */
    private int               isDelete;

    /**
     * 环境
     */
    private String            env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
