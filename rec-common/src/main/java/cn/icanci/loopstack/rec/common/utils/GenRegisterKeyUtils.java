package cn.icanci.loopstack.rec.common.utils;

import cn.icanci.loopstack.rec.common.model.config.RegisterVO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 17:38
 */
public class GenRegisterKeyUtils {

    private static final String FORMAT = "%s#%s#%s#%s";

    public static String generateKey(RegisterVO register) {
        return String.format(FORMAT, register.getAppName(), register.getClientAddress(), register.getClientPort(), register.getDomain());
    }
}
