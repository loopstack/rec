package cn.icanci.loopstack.rec.common.model.socket;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 21:35
 */
public class RegisterDTO implements Serializable {
    private static final long serialVersionUID = -2030921699127783725L;

    /**
     * SDK 服务ip地址
     */
    private String            clientAddress;
    /**
     * SDK 服务端口地址
     */
    private int               clientPort;
    /**
     * SDK 服务服务名字
     */
    private String            appName;

    /**
     * 当前服务注册的domain
     */
    private String            domain;


    public RegisterDTO() {
    }

    public RegisterDTO(String clientAddress, int clientPort, String appName, String domain) {
        this.clientAddress = clientAddress;
        this.clientPort = clientPort;
        this.appName = appName;
        this.domain = domain;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
