package cn.icanci.loopstack.rec.common.enums;

/**
 * 规则执行是否中断标志
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/11 11:35
 */
public enum InterruptEnum {
                           /**
                            * TRUE
                            */
                           TRUE("TRUE", "TRUE(中断)"),
                           /**
                            * FALSE
                            */
                           FALSE("FALSE", "FALSE(不中断)"),;

    InterruptEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
