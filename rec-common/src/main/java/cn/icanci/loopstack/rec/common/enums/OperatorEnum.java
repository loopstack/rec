package cn.icanci.loopstack.rec.common.enums;

/**
 * 操作符
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 09:57
 */
public enum OperatorEnum {
                          /** 等于 */
                          EQ("EQ", "等于(=)"),

                          /** 大于 */
                          GT("GT", "大于(>)"),

                          /** 大于等于 */
                          GTE("GTE", "大于等于(>=)"),

                          /** 小于 */
                          LT("LT", "小于(<)"),

                          /** 小于等于 */
                          LTE("LTE", "小于等于(<=)"),

                          /** 不等于 */
                          NE("NE", "不等于(!=)"),

                          /**包含(⊇) */
                          CONTAIN("CONTAIN", "包含(⊇)"),

                          /** 不包含 */
                          UN_CONTAIN("UN_CONTAIN", "不包含"),
                          /**
                           * 包含于(⊆或⊂)
                           */
                          INCLUDED("INCLUDED", "包含于(⊆或⊂)"),;

    /** 编码 */
    private String code;

    /** 描述 */
    private String desc;

    OperatorEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
