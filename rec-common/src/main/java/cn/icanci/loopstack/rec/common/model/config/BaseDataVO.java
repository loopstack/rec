package cn.icanci.loopstack.rec.common.model.config;

import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ResultTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;

/**
 * 基础数据
 *
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:36
 */
public class BaseDataVO extends BaseVO {
    /**
     * 域Code
     */
    private String         domainCode;
    /**
     * 基础数据名称
     */
    private String         fieldName;
    /**
     * 数据类型(布尔、字符串、数值、日期、元数据等)
     */
    private DataTypeEnum   dataType;
    /**
     * 关联的元数据uuid
     * 
     * {@link DataTypeEnum#METADATA}
     */
    private String         metadataUuid;
    /**
     * 脚本执行类型
     */
    private ScriptTypeEnum scriptType;
    /**
     * 脚本内容
     */
    private String         scriptContent;
    /**
     * 脚本执行返回类型(只能是基本数据类型)
     */
    private ResultTypeEnum resultType;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }


    public DataTypeEnum getDataType() {
        return dataType;
    }

    public void setDataType(DataTypeEnum dataType) {
        this.dataType = dataType;
    }

    public String getMetadataUuid() {
        return metadataUuid;
    }

    public void setMetadataUuid(String metadataUuid) {
        this.metadataUuid = metadataUuid;
    }

    public ScriptTypeEnum getScriptType() {
        return scriptType;
    }

    public void setScriptType(ScriptTypeEnum scriptType) {
        this.scriptType = scriptType;
    }

    public String getScriptContent() {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
    }

    public ResultTypeEnum getResultType() {
        return resultType;
    }

    public void setResultType(ResultTypeEnum resultType) {
        this.resultType = resultType;
    }
}
