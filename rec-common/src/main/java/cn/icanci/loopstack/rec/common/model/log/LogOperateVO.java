package cn.icanci.loopstack.rec.common.model.log;

import cn.icanci.loopstack.rec.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ModuleTypeEnum;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 21:13
 */
public class LogOperateVO {
    /** 编号 */
    private String              id;

    /** 操作模块 */
    private ModuleTypeEnum      module;

    /** 对象编号 */
    private String              targetId;

    /** 
     * 操作类型
     */
    private LogOperatorTypeEnum operatorType;

    /** 操作内容 */
    private String              content;

    /** 创建时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date                createTime;

    /** 环境 */
    String                      env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModuleTypeEnum getModule() {
        return module;
    }

    public void setModule(ModuleTypeEnum module) {
        this.module = module;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public LogOperatorTypeEnum getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(LogOperatorTypeEnum operatorType) {
        this.operatorType = operatorType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
