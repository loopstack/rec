package cn.icanci.loopstack.rec.common.model.report;

import java.util.Date;

/**
 * 执行日志模型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/06 22:45
 */
public class ReportVO {
    /**
     * 文档id
     */
    private String id;

    /**
     * 创建时间
     */
    private Date   createTime;

    /**
     * 更新时间
     */
    private Date   updateTime;

    /**
     * 环境
     */
    private String env;
}
