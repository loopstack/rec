package cn.icanci.loopstack.rec.common.result;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * 通用返回结果
 *
 * @author icanci
 * @since 1.0 Created in 2022/04/04 19:11
 */
public class R implements Serializable {
    private static final long   serialVersionUID = -1343013883236338104L;
    /** 是否成功 */
    private boolean             ok;
    /** 错误码 */
    private int                 code;
    /** 错误信息 */
    private String              message;
    /** 返回前端数据 */
    private Map<String, Object> data             = new HashMap<>();

    public R() {
    }

    /**
     * Builder
     * 
     * @return Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * BuilderOK
     *
     * @return Builder
     */
    public static Builder builderOk() {
        return new Builder().BuilderOK();
    }

    /**
     * BuilderFail
     *
     * @return Builder
     */
    public static Builder builderFail() {
        return new Builder().BuilderFail();
    }

    public static class Builder {
        /** 是否成功 */
        private boolean             ok;
        /** 错误码 */
        private int                 code;
        /** 错误信息 */
        private String              message;
        /** 返回前端数据 */
        private Map<String, Object> data = new HashMap<String, Object>();

        private Builder() {

        }

        public Builder(boolean ok, int code) {
            this.ok = ok;
            this.code = code;
        }

        private Builder BuilderOK() {
            this.ok = true;
            this.code = ResultCodes.SUCCESS;
            return this;
        }

        private Builder BuilderFail() {
            this.ok = false;
            this.code = ResultCodes.FAIL_SYSTEM;
            return this;
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Builder code(Integer val) {
            code = val;
            return this;
        }

        public Builder data(String key, Object value) {
            data.put(key, value);
            return this;
        }

        public Builder data(Map<String, Object> map) {
            data = map;
            return this;
        }

        public R build() {
            return new R(this);
        }
    }

    private R(Builder builder) {
        this.ok = builder.ok;
        this.code = builder.code;
        this.message = builder.message;
        this.data = builder.data;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("ok=" + ok).add("code=" + code).add("message=" + message).add("data=" + data).toString();
    }
}
