package cn.icanci.loopstack.rec.common.enums;

/**
 * 数据类型(布尔、字符串、数值、日期、元数据等)
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:41
 */
public enum DataTypeEnum {
                          /**
                           * 布尔
                           */
                          BOOLEAN("BOOLEAN", "布尔"),
                          /**
                           * 字符串
                           */
                          STRING("STRING", "字符串"),
                          /**
                           * 数值
                           */
                          NUMBER("NUMBER", "数值"),
                          /**
                           * 日期
                           */
                          DATE("DATE", "日期"),
                          /**
                           * 元数据
                           */
                          METADATA("METADATA", "元数据"),

    ;

    DataTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
