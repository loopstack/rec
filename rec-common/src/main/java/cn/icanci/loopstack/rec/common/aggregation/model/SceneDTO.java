package cn.icanci.loopstack.rec.common.aggregation.model;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 17:16
 */
public class SceneDTO extends BaseDTO {
    private static final long serialVersionUID = 6236086133690446406L;

    /**
     * 域Code
     */
    private String            domainCode;
    /**
     * 场景对
     */
    private List<ScenePair>   scenePairs;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public List<ScenePair> getScenePairs() {
        return scenePairs;
    }

    public void setScenePairs(List<ScenePair> scenePairs) {
        this.scenePairs = scenePairs;
    }

    /**
     * 场景对
     */
    public static class ScenePair {
        /**
         * 场景名称
         */
        private String sceneName;
        /**
         * 场景Code
         */
        private String sceneCode;
        /**
         * 唯一id用作数据关联
         */
        private String uniqueId;

        public String getSceneName() {
            return sceneName;
        }

        public void setSceneName(String sceneName) {
            this.sceneName = sceneName;
        }

        public String getSceneCode() {
            return sceneCode;
        }

        public void setSceneCode(String sceneCode) {
            this.sceneCode = sceneCode;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }
    }
}
