package cn.icanci.loopstack.rec.common.enums;

/**
 * 日志操作模块类型
 *
 * @author icanci
 * @since 1.0 Created in 2022/11/12 15:03
 */
public enum ModuleTypeEnum {

                            /**
                             * REC_DOMAIN
                             */
                            REC_DOMAIN("REC_DOMAIN", "规则域"),
                            /**
                             * REC_METADATA
                             */
                            REC_METADATA("REC_METADATA", "规则元数据"),
                            /**
                             * REC_BASE_DATA
                             */
                            REC_BASE_DATA("REC_BASE_DATA", "规则基础数据"),
                            /**
                             * REC_DATA_SOURCE
                             */
                            REC_DATA_SOURCE("REC_DATA_SOURCE", "规则数据源"),
                            /**
                             * REC_STRATEGY
                             */
                            REC_STRATEGY("REC_STRATEGY", "规则策略"),

    ;

    ModuleTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
