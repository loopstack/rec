package cn.icanci.loopstack.rec.common.aggregation;

import java.io.Serializable;
import java.util.Set;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/19 10:22
 */
public class WebApiRequest implements Serializable {
    private static final long serialVersionUID = 5046682826770853155L;

    private Set<String>       domainCodes;

    public WebApiRequest() {
    }

    public WebApiRequest(Set<String> domainCodes) {
        this.domainCodes = domainCodes;
    }

    public Set<String> getDomainCodes() {
        return domainCodes;
    }

    public void setDomainCodes(Set<String> domainCodes) {
        this.domainCodes = domainCodes;
    }
}
