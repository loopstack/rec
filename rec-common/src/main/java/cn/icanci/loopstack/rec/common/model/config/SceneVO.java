package cn.icanci.loopstack.rec.common.model.config;

import java.util.List;

/**
 * 场景
 *
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:14
 */
public class SceneVO extends BaseVO {
    /**
     * 域Code
     */
    private String          domainCode;
    /**
     * 场景对
     */
    private List<ScenePair> scenePairs;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public List<ScenePair> getScenePairs() {
        return scenePairs;
    }

    public void setScenePairs(List<ScenePair> scenePairs) {
        this.scenePairs = scenePairs;
    }

    /**
     * 场景对
     */
    public static class ScenePair {
        /**
         * 场景名称
         */
        private String sceneName;
        /**
         * 场景Code
         */
        private String sceneCode;

        public String getSceneName() {
            return sceneName;
        }

        public void setSceneName(String sceneName) {
            this.sceneName = sceneName;
        }

        public String getSceneCode() {
            return sceneCode;
        }

        public void setSceneCode(String sceneCode) {
            this.sceneCode = sceneCode;
        }
    }
}
