package cn.icanci.loopstack.rec.common.utils;

import cn.hutool.core.date.DateUtil;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 时间工具类
 *    
 * @author icanci
 * @since 1.0 Created in 2022/11/16 23:06
 */
public final class DateUtils {

    public enum FormatType {
                            /** YYYY */
                            YYYY("yyyy"),

                            /** MM_DD */
                            MM_DD("MM-dd"),

                            /** HH_MM */
                            HH_MM("HH:mm"),

                            /** HH_MM_SS */
                            HH_MM_SS("HH:mm:ss"),

                            /** YYYY_MM */
                            YYYY_MM("yyyy-MM"),

                            /** YYYY_MM_DD */
                            YYYY_MM_DD("yyyy-MM-dd"),

                            /** YYYY_MM_DD_HH */
                            YYYY_MM_DD_HH("yyyy-MM-dd HH"),

                            /** YYYY_MM_DD_HH_MM */
                            YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm"),

                            /** YYYY_MM_DD_HH_MM_SS */
                            YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),

        ;

        private final String code;

        public String getCode() {
            return code;
        }

        FormatType(String code) {
            this.code = code;
        }
    }

    /**
     * 转换日期
     * TODO Hutool 的时间格式化是线程安全的，因为每次都创建一个SimpleDateFormat对象，但是性能略差
     * 
     * @param dateString the time
     * @param formatType formatType
     * @return date
     */
    public static Date parse(String dateString, FormatType formatType) {
        if (StringUtils.isBlank(dateString)) {
            return null;
        }
        return DateUtil.parse(dateString, formatType.getCode());
    }

    /**
     * 格式化日期
     *
     * @param date the time
     * @param formatType formatType
     * @return date
     */
    public static String format(Date date, FormatType formatType) {
        if (formatType == null) {
            return null;
        }
        try {
            return DateUtil.format(date, formatType.getCode());
        } catch (Exception e) {
            return null;
        }
    }
}