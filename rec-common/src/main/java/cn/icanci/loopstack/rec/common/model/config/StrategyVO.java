package cn.icanci.loopstack.rec.common.model.config;

import cn.icanci.loopstack.rec.common.enums.*;

import java.util.List;

/**
 * 策略
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:39
 */
public class StrategyVO extends BaseVO {
    /**
     * 域Code
     */
    private String       domainCode;
    /**
     * 场景Code
     */
    private String       sceneCode;
    /**
     * 策略组名称
     */
    private String       strategyName;

    /**
     * 数据源关联uuid
     */
    private String       dataSourceUuid;
    /**
     * 规则配置类型(默认为List)
     */
    private RuleTypeEnum ruleType;
    /**
     * 规则模式 默认为simple
     */
    private RuleModeEnum ruleMode;
    /**
     * 规则配置类型为List时候的规则数据
     */
    private RuleListInfo ruleListInfo;
    /**
     * 规则配置类型为Tree时候的规则数据
     */
    private RuleTreeInfo ruleTreeInfo;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getDataSourceUuid() {
        return dataSourceUuid;
    }

    public void setDataSourceUuid(String dataSourceUuid) {
        this.dataSourceUuid = dataSourceUuid;
    }

    public RuleTypeEnum getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleTypeEnum ruleType) {
        this.ruleType = ruleType;
    }

    public RuleModeEnum getRuleMode() {
        return ruleMode;
    }

    public void setRuleMode(RuleModeEnum ruleMode) {
        this.ruleMode = ruleMode;
    }

    public RuleListInfo getRuleListInfo() {
        return ruleListInfo;
    }

    public void setRuleListInfo(RuleListInfo ruleListInfo) {
        this.ruleListInfo = ruleListInfo;
    }

    public RuleTreeInfo getRuleTreeInfo() {
        return ruleTreeInfo;
    }

    public void setRuleTreeInfo(RuleTreeInfo ruleTreeInfo) {
        this.ruleTreeInfo = ruleTreeInfo;
    }

    /**
     * 规则配置类型为List时候的规则数据
     */
    public static class RuleListInfo {
        /**
         * 平级结构
         *
         * 多个conditions为或的关系
         */
        private List<Condition> conditions;

        public List<Condition> getConditions() {
            return conditions;
        }

        public void setConditions(List<Condition> conditions) {
            this.conditions = conditions;
        }
    }

    /**
     * 平级结构
     */
    public static class Condition {
        /**
         * 组集合
         * 每一组内为且的关系
         */
        private List<ConditionCell> group;

        public List<ConditionCell> getGroup() {
            return group;
        }

        public void setGroup(List<ConditionCell> group) {
            this.group = group;
        }
    }

    /**
     * 单元结构
     */
    public static class ConditionCell {

        /** 单元名称 */
        private String         name;

        /** 左值：基础数据uuid */
        private String         leftValue;

        /** 操作符 */
        private OperatorEnum operator;

        /** 右值 */
        private String         rightValue;

        /** 是否中断执行 */
        private InterruptEnum interrupt;

        /**
         * 命中到当前条件的返回值类型
         *
         * 满足 {@link InterruptEnum#TRUE } 时候返回值
         */
        private ResultTypeEnum resultType;
        /**  
         * 命中到当前条件的返回值
         * 
         * 满足 {@link InterruptEnum#TRUE } 时候返回值
         */
        private String         returnVal;

        /**
         * 当前规则的子配置项
         * 一个规则自配置可以有多组，每组可能有多个，每组的每个也是一个配置项
         *
         * Tree 结构的 ConditionCell#children 会存储值
         * 非Tree结构则为空数组
         */
        List<ConditionCell>    children;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLeftValue() {
            return leftValue;
        }

        public void setLeftValue(String leftValue) {
            this.leftValue = leftValue;
        }

        public OperatorEnum getOperator() {
            return operator;
        }

        public void setOperator(OperatorEnum operator) {
            this.operator = operator;
        }

        public String getRightValue() {
            return rightValue;
        }

        public void setRightValue(String rightValue) {
            this.rightValue = rightValue;
        }

        public List<ConditionCell> getChildren() {
            return children;
        }

        public void setChildren(List<ConditionCell> children) {
            this.children = children;
        }

        public InterruptEnum getInterrupt() {
            return interrupt;
        }

        public void setInterrupt(InterruptEnum interrupt) {
            this.interrupt = interrupt;
        }

        public ResultTypeEnum getResultType() {
            return resultType;
        }

        public void setResultType(ResultTypeEnum resultType) {
            this.resultType = resultType;
        }

        public String getReturnVal() {
            return returnVal;
        }

        public void setReturnVal(String returnVal) {
            this.returnVal = returnVal;
        }
    }

    /**
     * 规则配置类型为Tree时候的规则数据
     */
    public static class RuleTreeInfo {
        /**
         * 多个conditions为或的关系
         */
        private List<Condition> conditions;

        public List<Condition> getConditions() {
            return conditions;
        }

        public void setConditions(List<Condition> conditions) {
            this.conditions = conditions;
        }
    }
}
