package cn.icanci.loopstack.rec.common.enums;

/**
 * 规则配置类型
 *
 * @author icanci
 * @since 1.0 Created in 2022/10/30 09:03
 */
public enum RuleTypeEnum {
                          /**
                           * LIST
                           */
                          LIST("LIST", "LIST"),
    // TODO 一期版本不处理TREE
    //                          /**
    //                           * TREE
    //                           */
    //                          TREE("TREE", "TREE"),

    ;

    private String code;
    private String desc;

    RuleTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
