package cn.icanci.loopstack.rec.common.enums;

/**
 * 脚本执行返回类型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:56
 */
public enum ResultTypeEnum {
                            /**
                             * Integer
                             */
                            INTEGER("INTEGER", "Integer"),
                            /**
                             * Long
                             */
                            LONG("LONG", "Long"),
                            /**
                             * Double
                             */
                            DOUBLE("DOUBLE", "Double"),
                            /**
                             * String
                             */
                            STRING("STRING", "String"),

    ;

    private String code;
    private String desc;

    ResultTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
