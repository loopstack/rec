package cn.icanci.loopstack.rec.common.model.config;

import java.util.Date;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 21:35
 */
public class RegisterVO extends BaseVO {
    /**
     * SDK 服务ip地址
     */
    private String clientAddress;
    /**
     * SDK 服务端口地址
     */
    private int    clientPort;
    /**
     * SDK 服务服务名字
     */
    private String appName;
    /**
     * 服务注册时间
     */
    private Date   registerTime;
    /**
     * 上次注册更新时间
     */
    private Date   lastUpdateTime;
    /**
     * 注册绑定的domain
     */
    private String domain;

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
