package cn.icanci.loopstack.rec.common.model.socket;

import java.io.Serializable;
import java.util.Set;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 21:17
 */
public class PublishDTO implements Serializable {
    private static final long serialVersionUID = 1223193630071803254L;

    private Set<String>       domainCodes;

    public Set<String> getDomainCodes() {
        return domainCodes;
    }

    public void setDomainCodes(Set<String> domainCodes) {
        this.domainCodes = domainCodes;
    }
}
