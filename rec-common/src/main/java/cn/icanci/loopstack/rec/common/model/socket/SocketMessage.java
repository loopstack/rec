package cn.icanci.loopstack.rec.common.model.socket;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 21:01
 */
@SuppressWarnings("all")
public class SocketMessage<T> {
    private boolean success;
    private String  errorMessage;
    private T       content;

    public static SocketMessage fail(String errorMessage) {
        SocketMessage message = new SocketMessage();
        message.setSuccess(false);
        message.setErrorMessage(errorMessage);
        return message;
    }

    public static SocketMessage success() {
        SocketMessage message = new SocketMessage();
        message.setSuccess(true);
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
