package cn.icanci.loopstack.rec.common.enums;

/**
 * HTTP 请求数据类型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:11
 */
public enum HttpRequestTypeEnum {
                                 /**
                                  * GET
                                  */
                                 GET("GET", "GET"),
                                 /**
                                  * POST
                                  */
                                 POST("POST", "POST"),

    ;

    HttpRequestTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
