package cn.icanci.loopstack.rec.common.model.config;

import java.util.List;

/**
 * 元数据
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:22
 */
public class MetadataVO extends BaseVO {
    /**
     * 域Code
     */
    private String             domainCode;
    /**
     * 元数据名称
     */
    private String             metadataName;
    /**
     * 元数据对
     */
    private List<MetadataPair> metadataPairs;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public List<MetadataPair> getMetadataPairs() {
        return metadataPairs;
    }

    public void setMetadataPairs(List<MetadataPair> metadataPairs) {
        this.metadataPairs = metadataPairs;
    }

    /**
     * 元数据对
     */
    public static class MetadataPair {
        /**
         * key
         */
        private String key;
        /**
         * value
         */
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
