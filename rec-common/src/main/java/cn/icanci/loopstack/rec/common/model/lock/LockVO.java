package cn.icanci.loopstack.rec.common.model.lock;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 16:38
 */
public class LockVO {
    /**
     * 文档id
     */
    private String id;
    /**
     * 过期时间
     */
    private long   expireAt;
    /**
     * token
     */
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(long expireAt) {
        this.expireAt = expireAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
