package cn.icanci.loopstack.rec.common.enums;

/**
 * 数据源类型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:08
 */
public enum DataSourceTypeEnum {

                                /**
                                 * SCRIPT
                                 */
                                SCRIPT("SCRIPT", "脚本"),
                                /**
                                 * HTTP
                                 */
                                HTTP("HTTP", "HTTP"),

    // SQL 的方式需要动态加载数据源、数据库等，第一发行版暂不处理
    // /**
    //  * SQL
    //  */
    // SQL("SQL", "SQL"),

    ;

    DataSourceTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
