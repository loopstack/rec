package cn.icanci.loopstack.rec.common.enums;

/**
 * 脚本类型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:48
 */
public enum ScriptTypeEnum {

                            /**
                             * GROOVY
                             */
                            GROOVY("GROOVY", "Groovy脚本"),
                            /**
                             * MVEL2
                             */
                            MVEL2("MVEL2", "Mvel2.0脚本"),
    //  TODO JAVA_SCRIPT
    //  /**
    //   * JAVA_SCRIPT
    //   */
    //  JAVA_SCRIPT("JAVA_SCRIPT", "JavaScript脚本"),
    // /**
    //  * Json 取值
    //  */
    // JSON("JSON", "Json取值"),

    ;

    private String code;
    private String desc;

    ScriptTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
