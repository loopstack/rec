package cn.icanci.loopstack.rec.common.model.socket;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 21:06
 */
public interface UriConstant {
    /** 心跳   */
    String HEARTBEAT = "/rec/register/heartbeat";
    /** 刷新 */
    String REFRESH   = "/rec/register/refresh";
    /** 注册 */
    String REGISTER  = "/rec/register/doRegister";
}
