package cn.icanci.loopstack.rec.common.enums;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 21:50
 */
public enum RuleModeEnum {
                          /**
                           * SIMPLE
                           */
                          SIMPLE("SIMPLE", "简单"),
                          /**
                           * COMPLEX
                           */
                          COMPLEX("COMPLEX", "复杂"),

    ;

    private final String code;
    private final String desc;

    RuleModeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
