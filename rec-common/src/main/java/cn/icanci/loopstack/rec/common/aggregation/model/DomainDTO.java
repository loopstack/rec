package cn.icanci.loopstack.rec.common.aggregation.model;

/**
 * 域网络传输模型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/15 15:44
 */
public class DomainDTO extends BaseDTO {
    private static final long serialVersionUID = -7107977844423682947L;

    /**
     * 域名称
     */
    private String            domainName;
    /**
     * 域Code
     */
    private String            domainCode;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }
}
