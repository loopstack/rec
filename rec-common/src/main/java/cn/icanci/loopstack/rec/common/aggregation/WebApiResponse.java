package cn.icanci.loopstack.rec.common.aggregation;

import java.io.Serializable;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/19 10:25
 */
public class WebApiResponse<T> implements Serializable {

    private static final long serialVersionUID = -7238440183222443753L;

    private List<T>           ret;

    public WebApiResponse(List<T> ret) {
        this.ret = ret;
    }

    public List<T> getRet() {
        return ret;
    }

    public void setRet(List<T> ret) {
        this.ret = ret;
    }
}
