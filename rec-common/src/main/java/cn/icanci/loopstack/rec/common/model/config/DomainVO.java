package cn.icanci.loopstack.rec.common.model.config;

/**
 * 域
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:13
 */
public class DomainVO extends BaseVO {
    /**
     * 域名称
     */
    private String domainName;
    /**
     * 域Code
     */
    private String domainCode;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }
}
