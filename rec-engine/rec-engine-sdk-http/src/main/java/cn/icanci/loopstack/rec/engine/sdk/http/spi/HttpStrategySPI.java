package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.StrategySPI;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:29
 */
public class HttpStrategySPI extends AbstractLoadSPI implements StrategySPI {
    @Override
    public List<StrategyDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(StrategyDTO.class, LOAD_STRATEGY_PATH, Method.POST, request).getRet();
    }

    @Override
    public List<StrategyDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }

}
