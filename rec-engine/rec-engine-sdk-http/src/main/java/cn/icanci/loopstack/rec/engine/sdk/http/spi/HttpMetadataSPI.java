package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.MetadataDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.MetadataSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:27
 */
public class HttpMetadataSPI extends AbstractLoadSPI implements MetadataSPI {
    @Override
    public List<MetadataDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(MetadataDTO.class, LOAD_METADATA_PATH, Method.POST, request).getRet();
    }

    @Override
    public List<MetadataDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }
}
