package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.BaseDataSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:26
 */
public class HttpBaseDataSPI extends AbstractLoadSPI implements BaseDataSPI {

    @Override
    public List<BaseDataDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(BaseDataDTO.class, LOAD_BASE_DATA_PATH, Method.POST, request).getRet();
    }

    @Override
    public List<BaseDataDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }
}
