package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.SceneDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.SceneSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:28
 */
public class HttpSceneSPI extends AbstractLoadSPI implements SceneSPI {
    @Override
    public List<SceneDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(SceneDTO.class, LOAD_SCENE_PATH, Method.POST, request).getRet();
    }

    @Override
    public List<SceneDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }

    @Override
    public SceneDTO loadOne(String domainCode) {
        return load(domainCode).iterator().next();
    }
}
