package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.DomainDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.DomainSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:02
 */
public class HttpDomainSPI extends AbstractLoadSPI implements DomainSPI {

    @Override
    public DomainDTO loadOne(String domain) {
        return load(domain).iterator().next();
    }

    @Override
    public Set<String> loadAllDomainCodes() {
        return Sets.newHashSet(call(String.class, LOAD_DOMAIN_CODES_PATH, Method.POST, null).getRet());
    }

    @Override
    public List<DomainDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(DomainDTO.class, LOAD_DOMAIN_PATH, Method.POST, request).getRet();

    }

    @Override
    public List<DomainDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }
}
