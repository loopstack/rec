package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.WebApiResponse;
import cn.icanci.loopstack.rec.common.utils.PropertiesUtil;
import cn.icanci.loopstack.rec.engine.script.client.Client;
import cn.icanci.loopstack.rec.engine.script.client.http.HttpClientImpl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Maps;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:35
 */
public abstract class AbstractLoadSPI {

    private static final String   PROPERTIES_NAME              = "rec-http-spi-load.properties";

    private static final String   REQ_HTTP_PATH_PROP           = "rec-http-request-url";
    /** 服务请求请求路径 */
    protected static final String REQ_HTTP_PATH;
    /** 域请求路径  */
    protected static String       LOAD_DOMAIN_PATH;
    /** 域Codes请求路径  */
    protected static String       LOAD_DOMAIN_CODES_PATH;
    private static String         LOAD_DOMAIN_PATH_SUFFIX      = "/rec/webapi/loadDomains";
    private static String         LOAD_DOMAIN_CODE_PATH_SUFFIX = "/rec/webapi/loadDomainCodes";
    /** 场景请求路径 */
    protected static String       LOAD_SCENE_PATH;
    private static String         LOAD_SCENE_PATH_SUFFIX       = "/rec/webapi/loadScenes";
    /** 元数据请求路径 */
    protected static String       LOAD_METADATA_PATH;
    private static String         LOAD_METADATA_PATH_SUFFIX    = "/rec/webapi/loadMetadatas";
    /** 基础数据请求路径  */
    protected static String       LOAD_BASE_DATA_PATH;
    private static String         LOAD_BASE_DATA_PATH_SUFFIX   = "/rec/webapi/loadBaseDatas";
    /** 数据源请求路径  */
    protected static String       LOAD_DATA_SOURCE_PATH;
    private static String         LOAD_DATA_SOURCE_PATH_SUFFIX = "/rec/webapi/loadDataSources";
    /** 策略请求路径  */
    protected static String       LOAD_STRATEGY_PATH;
    private static String         LOAD_STRATEGY_PATH_SUFFIX    = "/rec/webapi/loadStrategies";

    /** http实例 */
    protected static final Client CLIENT                       = HttpClientImpl.getInstance();

    /** DEFAULT_TIMEOUT */
    private static final int      DEFAULT_TIMEOUT              = 3;

    static {
        // 加载数据
        Map<String, String> propertyMap = PropertiesUtil.getPropertyMap(Thread.currentThread().getContextClassLoader(), PROPERTIES_NAME);
        REQ_HTTP_PATH = propertyMap.get(REQ_HTTP_PATH_PROP);

        // 请求路径加载
        LOAD_DOMAIN_PATH = REQ_HTTP_PATH + LOAD_DOMAIN_PATH_SUFFIX;
        LOAD_DOMAIN_CODES_PATH = REQ_HTTP_PATH + LOAD_DOMAIN_CODE_PATH_SUFFIX;
        LOAD_SCENE_PATH = REQ_HTTP_PATH + LOAD_SCENE_PATH_SUFFIX;
        LOAD_METADATA_PATH = REQ_HTTP_PATH + LOAD_METADATA_PATH_SUFFIX;
        LOAD_BASE_DATA_PATH = REQ_HTTP_PATH + LOAD_BASE_DATA_PATH_SUFFIX;
        LOAD_DATA_SOURCE_PATH = REQ_HTTP_PATH + LOAD_DATA_SOURCE_PATH_SUFFIX;
        LOAD_STRATEGY_PATH = REQ_HTTP_PATH + LOAD_STRATEGY_PATH_SUFFIX;
    }

    /**
     * 发送请求
     *
     * @param clazz 请求返回类型
     * @param request 请求参数
     * @param reqPath 请求地址
     * @param <T> 泛型
     * @return 返回执行结果
     */
    @SuppressWarnings("all")
    public <T> WebApiResponse<T> call(Class<T> clazz, String reqPath, Method httpMethod, WebApiRequest request) {

        Client.RpcRequest rpcRequest = new Client.RpcRequest(reqPath, request, Maps.newHashMap(), httpMethod, DEFAULT_TIMEOUT, TimeUnit.SECONDS, 3);

        List ret = CLIENT.call(rpcRequest, WebApiResponse.class).getRet();
        return new WebApiResponse<>(JSONUtil.toList(JSONUtil.parseArray(ret), clazz));
    }
}
