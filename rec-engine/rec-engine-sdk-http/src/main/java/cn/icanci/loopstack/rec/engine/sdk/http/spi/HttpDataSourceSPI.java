package cn.icanci.loopstack.rec.engine.sdk.http.spi;

import cn.hutool.http.Method;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.model.DataSourceDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.DataSourceSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:26
 */
public class HttpDataSourceSPI extends AbstractLoadSPI implements DataSourceSPI {
    @Override
    public List<DataSourceDTO> load(Set<String> domains) {
        WebApiRequest request = new WebApiRequest(domains);
        return call(DataSourceDTO.class, LOAD_DATA_SOURCE_PATH, Method.POST, request).getRet();
    }

    @Override
    public List<DataSourceDTO> load(String domain) {
        return load(Sets.newHashSet(domain));
    }
}
