package cn.icanci.loopstack.rec.engine.sdk.rule;

/**
 * 错误信息格式化
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/16 19:49
 */
public class ErrorMessageFormat {

    /** 根据 [域: %s][场景: %s] 查询策略不存在，不执行处理 */
    private static final String DOMAIN_AND_SCENE_NOT_FOUND_FORMAT = "根据 [域: %s][场景: %s] 查询策略不存在，不执行处理";
    /** 根据 [域: %s] 查询域不存在，不执行处理 */
    private static final String DOMAIN_FOUND_FORMAT               = "根据 [域: %s] 查询域不存在，不执行处理";
    /** 策略[%s::%s] 数据源[%s]  脚本执行失败：%s */
    private static final String DATA_SOURCE_EXECUTOR_FAIL_FORMAT  = "策略[%s::%s] 数据源[%s]  脚本执行失败：%s";

    public static String domainAndSceneNotFound(String domainCode, String sceneCode) {
        return String.format(DOMAIN_AND_SCENE_NOT_FOUND_FORMAT, domainCode, sceneCode);
    }

    public static String domainNotFound(String domainCode) {
        return String.format(DOMAIN_FOUND_FORMAT, domainCode);
    }

    public static String dataSourceExecutorFail(String domainName, String strategyName, String dataSourceName, String message) {
        return String.format(DATA_SOURCE_EXECUTOR_FAIL_FORMAT, domainName, strategyName, dataSourceName, message);
    }
}
