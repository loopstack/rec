package cn.icanci.loopstack.rec.engine.sdk.actuator.impl;

import cn.icanci.loopstack.rec.engine.sdk.actuator.RecRuleEngineActuator;
import cn.icanci.loopstack.rec.engine.sdk.actuator.RuleEngineRequest;
import cn.icanci.loopstack.rec.engine.sdk.actuator.RuleEngineResponse;
import cn.icanci.loopstack.rec.engine.sdk.rule.EngineExecutor;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 15:21
 */
@Service
public class RecRuleEngineActuatorImpl implements RecRuleEngineActuator {

    @Resource
    private EngineExecutor engineExecutor;

    @Override
    public RuleEngineResponse executor(RuleEngineRequest request) {
        return engineExecutor.execute(request);
    }
}
