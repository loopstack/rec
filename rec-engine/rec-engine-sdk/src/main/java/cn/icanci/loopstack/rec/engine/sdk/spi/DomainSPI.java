package cn.icanci.loopstack.rec.engine.sdk.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.DomainDTO;
import cn.icanci.loopstack.rec.engine.sdk.extensions.RecSpi;

import java.util.Set;

/**
 * 域加载SPI
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 09:11
 */
@RecSpi
public interface DomainSPI extends RecSupportSPI<DomainDTO> {

    /**
     * 加载数据
     *
     * @param domain 域
     * @return 返回相关的数据
     */
    DomainDTO loadOne(String domain);

    /**
     * 加载所有域Code
     * 
     * @return 返回所有域Code
     */
    Set<String> loadAllDomainCodes();
}
