package cn.icanci.loopstack.rec.engine.sdk.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 19:55
 */
public class ValidatorException extends RuntimeException {
    private static final long serialVersionUID = -512855499786214321L;

    public ValidatorException() {
        super();
    }

    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }

    protected ValidatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
