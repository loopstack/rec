package cn.icanci.loopstack.rec.engine.sdk.actuator;

import java.io.Serializable;

import javax.script.Bindings;

/**
 * 规则执行器返回结果
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 15:25
 */
public class RuleEngineResponse implements Serializable {
    private static final long serialVersionUID = -4658078915045778725L;
    /** 是否执行成功 */
    private boolean           success;
    /** 失败原因 */
    private String            errorMessage;
    /** 执行结果 */
    private Object            result;
    /** 执行请求参数 */
    private Bindings          bindings;

    public static RuleEngineResponse fail(String errorMessage) {
        RuleEngineResponse response = new RuleEngineResponse();
        response.setSuccess(false);
        response.setErrorMessage(errorMessage);
        return response;
    }

    public static RuleEngineResponse success(Object result, Bindings bindings) {
        RuleEngineResponse response = new RuleEngineResponse();
        response.setSuccess(true);
        response.setResult(result);
        response.setBindings(bindings);
        return response;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Bindings getBindings() {
        return bindings;
    }

    public void setBindings(Bindings bindings) {
        this.bindings = bindings;
    }
}
