package cn.icanci.loopstack.rec.engine.sdk.rule.repository;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 08:35
 */
public class DomainSceneKey {
    private String domainCode;
    private String sceneCode;

    public DomainSceneKey(String domainCode, String sceneCode) {
        this.domainCode = domainCode;
        this.sceneCode = sceneCode;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        DomainSceneKey that = (DomainSceneKey) o;
        return Objects.equals(domainCode, that.domainCode) && Objects.equals(sceneCode, that.sceneCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainCode, sceneCode);
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("domainCode=" + domainCode).add("sceneCode=" + sceneCode).toString();
    }
}
