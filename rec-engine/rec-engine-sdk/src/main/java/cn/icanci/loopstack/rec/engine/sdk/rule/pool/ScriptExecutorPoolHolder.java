package cn.icanci.loopstack.rec.engine.sdk.rule.pool;

import cn.icanci.loopstack.rec.engine.script.context.RecScriptEngineContext;

import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

/**
 * 脚本执行线程池处理器
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/16 21:06
 */
@Service
public class ScriptExecutorPoolHolder {

    private static final ScriptExecutorPoolHolder INSTANCE = new ScriptExecutorPoolHolder();

    public static ScriptExecutorPoolHolder getInstance() {
        return INSTANCE;
    }

    private final ThreadPoolExecutor registryOrRemoveThreadPool;

    private ScriptExecutorPoolHolder() {
        registryOrRemoveThreadPool = new ThreadPoolExecutor(40, 80, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(2000),
            r -> new Thread(r, "ScriptExecutorPoolHolderPool-" + r.hashCode()), (r, executor) -> r.run());
    }

    /**
     * 提交任务
     * 
     * @param task task
     */
    public void submit(FutureTask<RecScriptEngineContext<Object>> task) {
        registryOrRemoveThreadPool.submit(task);
    }
}
