package cn.icanci.loopstack.rec.engine.sdk.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 19:03
 */
public class RuleAggregationException extends RuntimeException {
    private static final long serialVersionUID = 7174187020377141821L;

    public RuleAggregationException() {
        super();
    }

    public RuleAggregationException(String message) {
        super(message);
    }

    public RuleAggregationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuleAggregationException(Throwable cause) {
        super(cause);
    }

    protected RuleAggregationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
