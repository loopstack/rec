package cn.icanci.loopstack.rec.engine.sdk.actuator;

/**
 * 规则引擎执行器入口
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 15:18
 */
public interface RecRuleEngineActuator {
    /**
     * 执行器执行
     * 
     * @param request request
     * @return 返回执行结果
     */
    RuleEngineResponse executor(RuleEngineRequest request);
}
