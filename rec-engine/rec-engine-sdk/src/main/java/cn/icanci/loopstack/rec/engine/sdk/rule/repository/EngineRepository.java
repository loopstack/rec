package cn.icanci.loopstack.rec.engine.sdk.rule.repository;

import cn.icanci.loopstack.rec.common.aggregation.model.*;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * EngineRepository Local Cache
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/15 22:56
 */
public final class EngineRepository {
    /** 域缓存 key:domainCode value:DomainDTO */
    private DomainDTO domainRepository;

    /** 基础数据缓存 key:uuid value:BaseDataDTO*/
    private Map<String, BaseDataDTO> baseDataRepository = Maps.newConcurrentMap();

    /** 元数据数据缓存 key:uuid value:MetadataDTO */
    private Map<String, MetadataDTO> metadataRepository = Maps.newConcurrentMap();

    /** 数据源缓存 key:uuid value:MetadataDTO*/
    private Map<String, DataSourceDTO> dataSourceRepository = Maps.newConcurrentMap();

    /** 数据源缓存 key:DomainSceneKey value:StrategyDTO*/
    private Map<DomainSceneKey, StrategyDTO> strategyRepository = Maps.newConcurrentMap();

    public DomainDTO getDomainRepository() {
        return domainRepository;
    }

    public void setDomainRepository(DomainDTO domainRepository) {
        this.domainRepository = domainRepository;
    }

    public Map<String, BaseDataDTO> getBaseDataRepository() {
        return baseDataRepository;
    }

    public void setBaseDataRepository(Map<String, BaseDataDTO> baseDataRepository) {
        this.baseDataRepository = baseDataRepository;
    }

    public Map<String, MetadataDTO> getMetadataRepository() {
        return metadataRepository;
    }

    public void setMetadataRepository(Map<String, MetadataDTO> metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    public Map<String, DataSourceDTO> getDataSourceRepository() {
        return dataSourceRepository;
    }

    public void setDataSourceRepository(Map<String, DataSourceDTO> dataSourceRepository) {
        this.dataSourceRepository = dataSourceRepository;
    }

    public Map<DomainSceneKey, StrategyDTO> getStrategyRepository() {
        return strategyRepository;
    }

    public void setStrategyRepository(Map<DomainSceneKey, StrategyDTO> strategyRepository) {
        this.strategyRepository = strategyRepository;
    }
}
