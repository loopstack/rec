package cn.icanci.loopstack.rec.engine.sdk.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 17:25
 */
@Component
@ConfigurationProperties(prefix = "rec")
public class RecProperties {

    /**
     * AppName信息
     */
    private String  appName;

    /**
     * 需要加载的域Code
     * 以英文,分隔
     */
    private String  domain;

    /**
     * 是否需要加载，不加载则不会注册并加载数据
     */
    private boolean load       = false;

    /**
     * 是否加载所有域，为true则加载所有域，为false则加载domain
     */
    private boolean loadAll    = false;
    /**
     * 加载的环境
     */
    private String  env        = "test";

    /**
     * 客户端注册的port
     */
    private int     clientPort = 11000;
    /**
     * 服务端ip，以,分隔
     */
    private String  serverIps  = "127.0.0.1";
    /**
     * 服务端port
     */
    private int     serverPort = 9999;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isLoad() {
        return load;
    }

    public void setLoad(boolean load) {
        this.load = load;
    }

    public boolean isLoadAll() {
        return loadAll;
    }

    public void setLoadAll(boolean loadAll) {
        this.loadAll = loadAll;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public String getServerIps() {
        return serverIps;
    }

    public void setServerIps(String serverIps) {
        this.serverIps = serverIps;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }
}
