package cn.icanci.loopstack.rec.engine.sdk.factor;

import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;

import javax.script.Bindings;

/**
 * 执行因子
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/16 22:03
 */
public interface Factor {
    /**
     * 是否匹配
     *
     * @param bindings 请求参数
     * @param conditionCell 条件
     * @param domainCode 域
     * @return 是否匹配
     */
    boolean match(Bindings bindings, StrategyDTO.ConditionCell conditionCell, String domainCode);
}
