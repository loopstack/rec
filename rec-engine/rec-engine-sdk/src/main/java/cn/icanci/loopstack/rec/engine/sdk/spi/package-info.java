/**
 * 定义加载数据的标准SPI接口
 * - 实现方式HTTP：通过HTTP远程调用加载
 * - 实现方式Mongo：通过关联MongoDB数据库进行加载
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 09:19
 */
package cn.icanci.loopstack.rec.engine.sdk.spi;