package cn.icanci.loopstack.rec.engine.sdk.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.BaseDTO;

import java.util.List;
import java.util.Set;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 09:11
 */
public interface RecSupportSPI<T extends BaseDTO> {
    /**
     * 加载数据
     *
     * @param domains 域
     * @return 返回相关的数据
     */
    List<T> load(Set<String> domains);

    /**
     * 加载数据
     *
     * @param domain 域
     * @return 返回相关的数据
     */
    List<T> load(String domain);
}
