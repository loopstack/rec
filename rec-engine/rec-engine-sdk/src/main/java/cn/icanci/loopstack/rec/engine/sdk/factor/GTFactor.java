package cn.icanci.loopstack.rec.engine.sdk.factor;

import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;
import cn.icanci.loopstack.rec.common.enums.OperatorEnum;
import cn.icanci.loopstack.rec.common.utils.DateUtils;

import java.math.BigDecimal;
import java.util.Objects;

import javax.script.Bindings;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 23:08
 */
@Component
@FactorBean(OperatorEnum.GT)
public class GTFactor extends AbstractFactor {

    @Override
    public boolean match(Bindings bindings, StrategyDTO.ConditionCell conditionCell, String domainCode) {
        BaseDataDTO baseData = engineRepositoryHolder.getBaseData(domainCode, conditionCell.getLeftValue());
        String leftValue = getValue(bindings, baseData);
        String rightValue = conditionCell.getRightValue();
        DataTypeEnum dataType = DataTypeEnum.valueOf(baseData.getDataType());
        switch (dataType) {
            case NUMBER:
                return new BigDecimal(leftValue).compareTo(new BigDecimal(rightValue)) > 0;
            case DATE:
                return Objects.requireNonNull(DateUtils.parse(leftValue, DateUtils.FormatType.YYYY_MM_DD_HH_MM_SS))
                    .compareTo(DateUtils.parse(rightValue, DateUtils.FormatType.YYYY_MM_DD_HH_MM_SS)) > 0;
            default:
                // no op
        }
        return false;
    }
}
