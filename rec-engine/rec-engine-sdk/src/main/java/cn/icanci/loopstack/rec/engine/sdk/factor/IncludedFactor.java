package cn.icanci.loopstack.rec.engine.sdk.factor;

import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;
import cn.icanci.loopstack.rec.common.enums.OperatorEnum;

import java.util.Arrays;
import java.util.List;

import javax.script.Bindings;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/17 09:24
 */
@Component
@FactorBean(OperatorEnum.INCLUDED)
public class IncludedFactor extends AbstractFactor {
    /** 右值分隔符 */
    private static final String RIGHT_SPLIT = "::";

    @Override
    public boolean match(Bindings bindings, StrategyDTO.ConditionCell conditionCell, String domainCode) {
        BaseDataDTO baseData = engineRepositoryHolder.getBaseData(domainCode, conditionCell.getLeftValue());
        String leftValue = getValue(bindings, baseData);
        String rightValue = conditionCell.getRightValue();
        DataTypeEnum dataType = DataTypeEnum.valueOf(baseData.getDataType());
        List<String> rightValueList = Arrays.asList(rightValue.split(RIGHT_SPLIT));
        switch (dataType) {
            case STRING:
            case METADATA:
                return rightValueList.contains(leftValue);
            default:
                // no op
        }
        return false;
    }
}
