package cn.icanci.loopstack.rec.engine.sdk.factor;

import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;
import cn.icanci.loopstack.rec.common.enums.OperatorEnum;

import javax.script.Bindings;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/17 09:24
 */
@Component
@FactorBean(OperatorEnum.CONTAIN)
public class ContainFactor extends AbstractFactor {

    @Override
    public boolean match(Bindings bindings, StrategyDTO.ConditionCell conditionCell, String domainCode) {
        BaseDataDTO baseData = engineRepositoryHolder.getBaseData(domainCode, conditionCell.getLeftValue());
        String leftValue = getValue(bindings, baseData);
        String rightValue = conditionCell.getRightValue();
        DataTypeEnum dataType = DataTypeEnum.valueOf(baseData.getDataType());

        switch (dataType) {
            case STRING:
            case METADATA:
                return leftValue.contains(rightValue);
            default:
                // no op
        }
        return false;
    }
}
