package cn.icanci.loopstack.rec.engine.sdk.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.SceneDTO;
import cn.icanci.loopstack.rec.engine.sdk.extensions.RecSpi;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 09:16
 */
@RecSpi
public interface SceneSPI extends RecSupportSPI<SceneDTO> {

    /**
     * 加载数据
     *
     * @param domainCode domainCode
     * @return 返回相关的数据
     */
    SceneDTO loadOne(String domainCode);
}
