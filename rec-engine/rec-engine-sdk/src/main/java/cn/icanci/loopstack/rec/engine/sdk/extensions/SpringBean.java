package cn.icanci.loopstack.rec.engine.sdk.extensions;

import java.lang.annotation.*;

/**
 * SPI的实现类中某些字段是否需要从Spring容器注入
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:42
 */
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SpringBean {
    /**
     * 需要被注入的bean的Class
     * 
     * @return 返回需要被注入的Bean的Class
     */
    Class<?>[] value();
}
