package cn.icanci.loopstack.rec.engine.sdk.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 23:15
 */
public class InjectionBeanException extends RuntimeException{
    private static final long serialVersionUID = 1755052174893509949L;

    public InjectionBeanException() {
        super();
    }

    public InjectionBeanException(String message) {
        super(message);
    }

    public InjectionBeanException(String message, Throwable cause) {
        super(message, cause);
    }

    public InjectionBeanException(Throwable cause) {
        super(cause);
    }

    protected InjectionBeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
