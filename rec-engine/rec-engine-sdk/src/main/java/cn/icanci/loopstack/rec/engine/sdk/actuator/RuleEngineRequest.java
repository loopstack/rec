package cn.icanci.loopstack.rec.engine.sdk.actuator;

import java.io.Serializable;
import java.util.Map;

/**
 * 执行引擎执行请求参数
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 15:22
 */
public class RuleEngineRequest implements Serializable {

    private static final long   serialVersionUID = -2598161333711352719L;

    /** 输入参数 Map 格式 */
    private Map<String, Object> parameters;

    /** 域 */
    private String              domainCode;

    /** 场景编码 */
    private String              sceneCode;

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }
}
