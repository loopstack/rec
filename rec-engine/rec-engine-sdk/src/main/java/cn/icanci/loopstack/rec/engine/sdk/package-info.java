/**
 * SDK 对接入方提供服务
 * 接入方接入SDK即可进行数据的推送和治理
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 15:13
 */
package cn.icanci.loopstack.rec.engine.sdk;