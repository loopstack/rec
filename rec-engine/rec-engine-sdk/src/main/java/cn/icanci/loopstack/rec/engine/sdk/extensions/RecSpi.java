package cn.icanci.loopstack.rec.engine.sdk.extensions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 规则引擎执行SPi标记
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:16
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RecSpi {
}
