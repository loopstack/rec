package cn.icanci.loopstack.rec.engine.sdk.rule;

import cn.icanci.loopstack.rec.common.aggregation.model.*;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 19:16
 */
public interface EngineRepositoryLoader {

    /**
     * 加载域信息
     * 
     * @param domainCode domainCode
     * @return 返回加载域信息
     */
    DomainDTO loadDomain(String domainCode);

    /**
     * 加载场景对信息
     *
     *
     * @param domainCode domainCode
     * @param sceneCode sceneCode
     * @return 返回场景对信息
     */
    SceneDTO.ScenePair loadScenePair(String domainCode, String sceneCode);

    /**
     * 加载场景对信息
     *
     * @param domainCode domainCode
     * @return 返回场景对信息
     */
    List<SceneDTO.ScenePair> loadScenePairs(String domainCode);

    /**
     * 加载基础数据
     * 
     * @param domainCode domainCode
     * @return 返回基础数据
     */
    List<BaseDataDTO> loadBaseDatas(String domainCode);

    /**
     * 加载元数据
     *
     * @param domainCode domainCode
     * @return 返回元数据
     */
    List<MetadataDTO> loadMetadatas(String domainCode);

    /**
     * 加载数据源信息
     *
     * @param domainCode domainCode
     * @return 返回数据源信息
     */
    List<DataSourceDTO> loadDataSource(String domainCode);

    /**
     * 加载执行策略
     * 
     * @param domainCode domainCode
     * @return 返回执行策略
     */
    List<StrategyDTO> loadStrategy(String domainCode);
}
