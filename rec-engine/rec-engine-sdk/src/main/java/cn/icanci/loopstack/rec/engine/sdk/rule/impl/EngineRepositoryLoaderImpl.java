package cn.icanci.loopstack.rec.engine.sdk.rule.impl;

import cn.icanci.loopstack.rec.common.aggregation.model.*;
import cn.icanci.loopstack.rec.engine.sdk.rule.EngineRepositoryLoader;
import cn.icanci.loopstack.rec.engine.sdk.spi.*;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/15 19:28
 */
@Service
public class EngineRepositoryLoaderImpl implements EngineRepositoryLoader {

    @Resource
    private DomainSPI     domainSPI;
    @Resource
    private SceneSPI      sceneSPI;
    @Resource
    private BaseDataSPI   baseDataSPI;
    @Resource
    private MetadataSPI   metadataSPI;
    @Resource
    private DataSourceSPI dataSourceSPI;
    @Resource
    private StrategySPI   strategySPI;

    @Override
    public DomainDTO loadDomain(String domainCode) {
        return domainSPI.loadOne(domainCode);
    }

    @Override
    public SceneDTO.ScenePair loadScenePair(String domainCode, String sceneCode) {
        SceneDTO scene = sceneSPI.loadOne(domainCode);
        Optional<SceneDTO.ScenePair> optional = scene.getScenePairs().stream().filter(pair -> StringUtils.equals(pair.getSceneCode(), sceneCode)).findFirst();
        return optional.orElse(null);
    }

    @Override
    public List<SceneDTO.ScenePair> loadScenePairs(String domainCode) {
        SceneDTO scene = sceneSPI.loadOne(domainCode);
        return scene.getScenePairs();
    }

    @Override
    public List<BaseDataDTO> loadBaseDatas(String domainCode) {
        return baseDataSPI.load(domainCode);
    }

    @Override
    public List<MetadataDTO> loadMetadatas(String domainCode) {
        return metadataSPI.load(domainCode);
    }

    @Override public List<DataSourceDTO> loadDataSource(String domainCode) {
        return dataSourceSPI.load(domainCode);
    }

    @Override
    public List<StrategyDTO> loadStrategy(String domainCode) {
        return strategySPI.load(domainCode);
    }
}
