package cn.icanci.loopstack.rec.engine.sdk.factor;

import cn.icanci.loopstack.rec.common.enums.OperatorEnum;

import java.util.Collection;
import java.util.Map;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 22:05
 */
@Service
public class FactorSupport implements ApplicationContextAware {
    /** 执行存储单元 */
    private static final Map<OperatorEnum, Factor> REPOSITORY = Maps.newHashMap();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, Factor> beansOfTypeMap = applicationContext.getBeansOfType(Factor.class);
        Collection<Factor> factors = beansOfTypeMap.values();
        for (Factor factor : factors) {
            FactorBean factorBean = AopUtils.getTargetClass(factor).getAnnotation(FactorBean.class);
            REPOSITORY.put(factorBean.value(), factor);
        }
    }

    /**
     * 获取Condition
     * 
     * @param operator operator
     * @return 返回Condition
     */
    public Factor getFactor(OperatorEnum operator) {
        return REPOSITORY.get(operator);
    }
}
