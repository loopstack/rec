package cn.icanci.loopstack.rec.engine.sdk.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.engine.sdk.extensions.RecSpi;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 09:16
 */
@RecSpi
public interface StrategySPI extends RecSupportSPI<StrategyDTO> {
}
