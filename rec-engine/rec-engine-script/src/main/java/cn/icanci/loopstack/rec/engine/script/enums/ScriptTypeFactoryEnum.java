package cn.icanci.loopstack.rec.engine.script.enums;

import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngineFactory;

import org.codehaus.groovy.jsr223.GroovyScriptEngineFactory;
import org.mvel2.jsr223.MvelScriptEngineFactory;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 12:07
 */
public enum ScriptTypeFactoryEnum {
                                   /**
                                    * GROOVY
                                    */
                                   GROOVY(ScriptTypeEnum.GROOVY, new GroovyScriptEngineFactory()),
                                   /**
                                    * MVEL2
                                    */
                                   MVEL2(ScriptTypeEnum.MVEL2, new MvelScriptEngineFactory()),

    /**
     * TODO JAVA_SCRIPT
     *
     * Tip: NashornScriptEngineFactory 支持执行
     *      nashorn,Nashorn,js,JS,JavaScript,javascript,ECMAScript,ecmascript
     *      此处只用到了JavaScript
     *      
     * Warning: Nashorn engine is planned to be removed from a future JDK release
     */
    // JAVA_SCRIPT(ScriptTypeEnum.JAVA_SCRIPT, new NashornScriptEngineFactory()),

    ;

    private static final Map<ScriptTypeEnum, ScriptEngineFactory> MAP = new HashMap<>();

    static {
        ScriptTypeFactoryEnum[] values = ScriptTypeFactoryEnum.values();
        for (ScriptTypeFactoryEnum value : values) {
            MAP.put(value.scriptType, value.factory);
        }
    }

    private final ScriptTypeEnum      scriptType;
    private final ScriptEngineFactory factory;

    /**
     * 根据 ScriptTypeEnum 获取对应的类型
     *
     * @param scriptType scriptType
     * @return 返回对应的类型
     */
    public static ScriptEngineFactory getScriptEngineFactoryByScriptType(ScriptTypeEnum scriptType) {
        return MAP.get(scriptType);
    }

    ScriptTypeFactoryEnum(ScriptTypeEnum scriptType, ScriptEngineFactory factory) {
        this.scriptType = scriptType;
        this.factory = factory;
    }

    public ScriptTypeEnum getScriptType() {
        return scriptType;
    }

    public ScriptEngineFactory getFactory() {
        return factory;
    }

}
