package cn.icanci.loopstack.rec.engine.script.exception;

/**
 * 脚本运行时异常
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/12 22:34
 */
public class ScriptRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 8247161532396425034L;

    public ScriptRuntimeException() {
        super();
    }

    public ScriptRuntimeException(String message) {
        super(message);
    }

    public ScriptRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScriptRuntimeException(Throwable cause) {
        super(cause);
    }

    protected ScriptRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
