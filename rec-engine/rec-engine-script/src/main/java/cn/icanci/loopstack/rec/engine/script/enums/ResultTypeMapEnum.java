package cn.icanci.loopstack.rec.engine.script.enums;

import cn.icanci.loopstack.rec.common.enums.ResultTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 类型映射
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 11:57
 */
public enum ResultTypeMapEnum {
                               /**
                                * INTEGER
                                */
                               INTEGER(ResultTypeEnum.INTEGER, Integer.class),
                               /**
                                * LONG
                                */
                               LONG(ResultTypeEnum.LONG, Long.class),
                               /**
                                * DOUBLE
                                */
                               DOUBLE(ResultTypeEnum.DOUBLE, Double.class),
                               /**
                                * STRING
                                */
                               STRING(ResultTypeEnum.STRING, String.class),

    ;

    private static final Map<ResultTypeEnum, Class> MAP = new HashMap<>();

    static {
        ResultTypeMapEnum[] values = ResultTypeMapEnum.values();
        for (ResultTypeMapEnum resultTypeMap : values) {
            MAP.put(resultTypeMap.resultType, resultTypeMap.clazz);
        }
    }

    private final ResultTypeEnum resultType;
    private final Class<?>       clazz;

    /**
     * 根据 ResultTypeEnum 获取对应的类型
     *
     * @param resultType resultType
     * @return 返回对应的类型
     */
    public static Class getClassByResultType(ResultTypeEnum resultType) {
        return MAP.get(resultType);
    }

    ResultTypeMapEnum(ResultTypeEnum resultType, Class clazz) {
        this.resultType = resultType;
        this.clazz = clazz;
    }

    public ResultTypeEnum getResultType() {
        return resultType;
    }

    public Class getClazz() {
        return clazz;
    }

}
