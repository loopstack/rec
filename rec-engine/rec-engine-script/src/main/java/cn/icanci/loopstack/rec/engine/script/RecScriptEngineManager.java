package cn.icanci.loopstack.rec.engine.script;

import cn.icanci.loopstack.rec.engine.script.impl.RecScriptEngineImpl;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 22:27
 */
public final class RecScriptEngineManager {

    /**
     * 获取 REC 执行引擎
     *
     * @return 返回 REC 执行引擎
     */
    public static RecScriptEngine getRecScriptEngine() {
        return RecScriptEngineHolder.INSTANCE;
    }

    private static class RecScriptEngineHolder {
        private static final RecScriptEngine INSTANCE = new RecScriptEngineImpl();
    }
}
