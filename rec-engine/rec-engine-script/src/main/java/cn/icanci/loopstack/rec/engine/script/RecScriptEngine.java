package cn.icanci.loopstack.rec.engine.script;

import cn.icanci.loopstack.rec.engine.script.wrapper.HttpResponseWrapper;
import cn.icanci.loopstack.rec.common.enums.HttpRequestTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;
import cn.icanci.loopstack.rec.engine.script.context.RecScriptEngineContext;

import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;

/**
 * 脚本执行引擎抽象顶级接口
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/12 22:12
 */
public interface RecScriptEngine {
    // ================================ 脚本编译 ================================

    /**
     * 根据脚本类型找到对应的执行引擎
     *
     * @param scriptType scriptType
     * @return 返回对应的执行引擎
     */
    ScriptEngine findEngine(ScriptTypeEnum scriptType);

    /**
     * 根据脚本类型和脚本进行编译
     *
     * @param scriptType scriptType
     * @param scriptContent scriptContent
     * @return 返回编译结果
     */
    CompiledScript compile(ScriptTypeEnum scriptType, String scriptContent);

    // ================================ 脚本执行 ================================
    /**
     * 执行脚本
     *
     * @param scriptType 脚本类型
     * @param script 脚本内容
     * @return 脚本执行返回上下文
     */
    RecScriptEngineContext<Object> eval(ScriptTypeEnum scriptType, String script);

    /**
     * 执行脚本
     *
     * @param scriptType 脚本类型
     * @param bindings 脚本运行时参数
     * @param script 脚本内容
     * @return 脚本执行返回上下文
     */
    RecScriptEngineContext<Object> eval(ScriptTypeEnum scriptType, Bindings bindings, String script);

    /**
     * 执行脚本
     *
     * @param scriptType 脚本类型
     * @param script 脚本内容
     * @param clazz 脚本执行返回类型
     * @param <T> 泛型
     * @return 脚本执行返回上下文
     */
    <T> RecScriptEngineContext<T> eval(ScriptTypeEnum scriptType, String script, Class<T> clazz);

    /**
     * 执行脚本
     *
     * @param scriptType 脚本类型
     * @param bindings 脚本运行时参数
     * @param script 脚本内容
     * @param clazz 脚本执行返回类型
     * @param <T> 泛型
     * @return 脚本执行返回上下文
     */
    <T> RecScriptEngineContext<T> eval(ScriptTypeEnum scriptType, Bindings bindings, String script, Class<T> clazz);

    // ================================ HTTP 请求执行 ================================
    /**
     * 执行HTTP请求
     *
     * @param requestType 请求类型
     * @param reqUrl 请求路径
     * @param reqParam 请求类型为POST的请求参数
     * @param timeout 请求超时时间
     * @return 返回请求执行结果
     */
    HttpResponseWrapper httpEval(HttpRequestTypeEnum requestType, String reqUrl, String reqParam, int timeout);

}
