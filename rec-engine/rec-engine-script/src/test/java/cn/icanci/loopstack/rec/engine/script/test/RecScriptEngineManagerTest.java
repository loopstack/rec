package cn.icanci.loopstack.rec.engine.script.test;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.rec.engine.script.RecScriptEngine;
import cn.icanci.loopstack.rec.engine.script.RecScriptEngineManager;
import cn.icanci.loopstack.rec.engine.script.wrapper.HttpResponseWrapper;
import cn.icanci.loopstack.rec.common.enums.HttpRequestTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;
import cn.icanci.loopstack.rec.engine.script.context.RecScriptEngineContext;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

import org.junit.Test;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 13:37
 */
public class RecScriptEngineManagerTest {
    @Test
    public void testRecScriptEngineManager() {
        RecScriptEngine recScriptEngine = RecScriptEngineManager.getRecScriptEngine();
        RecScriptEngineContext<Object> context = recScriptEngine.eval(ScriptTypeEnum.MVEL2, "1+2", Object.class);
        System.out.println(context);
        Object retVal = context.getRealRetVal();
        System.out.println(retVal);
        System.out.println(context);
    }

    @Test
    public void testRecScriptEngineManagerHttp() {
        RecScriptEngine recScriptEngine = RecScriptEngineManager.getRecScriptEngine();
        String reqUrl = "http://localhost:9110/dijiang/sys/config/query";
        ConfigRequest request = new ConfigRequest();
        request.setUk("xxx");
        request.setEnv("test");
        request.setFileName("hello");

        HttpResponseWrapper eval = recScriptEngine.httpEval(HttpRequestTypeEnum.POST, reqUrl, JSONUtil.toJsonStr(request), 3);
        if (eval.isSuccess()) {
            System.out.println(eval.isJson());
            System.out.println(eval.getResponseForMap());
        }
        System.out.println(eval);
    }

    @Test
    public void fastjsonIsJson() {
        //        System.out.println(FastJsonUtils.isJson("「9999"));
        System.out.println(JSONUtil.isJson("TRUE"));
        System.out.println(JSONUtil.isJson("false    "));
        //        System.out.println(FastJsonUtils.isJson("11.1"));
        System.out.println(Boolean.parseBoolean("true"));
        System.out.println(Boolean.parseBoolean("false"));
    }

    @Test
    public void testMvel2() {
        String script = "return {\"Jim\", \"Bob\", \"Smith\"}";
        RecScriptEngine recScriptEngine = RecScriptEngineManager.getRecScriptEngine();
        RecScriptEngineContext<Object> context = recScriptEngine.eval(ScriptTypeEnum.MVEL2, script, Object.class);
        System.out.println(context.getRealRetVal());
        System.out.println(context.getRealRetVal().getClass().isArray());
        Object[] arr = (Object[]) context.getRealRetVal();
        for (Object o : Arrays.asList(arr)) {
            System.out.println(o);
        }
        System.out.println(context.getRealRetVal() instanceof Map);
    }

    private static class ConfigRequest implements Serializable {

        private static final long serialVersionUID = 2380811183737977291L;
        /**
         * 项目唯一id
         */
        private String            uk;
        /**
         * 环境
         */
        private String            env;
        /**
         * 配置文件名字
         */
        private String            fileName;

        public String getUk() {
            return uk;
        }

        public void setUk(String uk) {
            this.uk = uk;
        }

        public String getEnv() {
            return env;
        }

        public void setEnv(String env) {
            this.env = env;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }
}
