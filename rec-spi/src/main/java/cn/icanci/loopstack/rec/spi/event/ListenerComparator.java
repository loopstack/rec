package cn.icanci.loopstack.rec.spi.event;

import java.util.Comparator;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 18:01
 */
public class ListenerComparator implements Comparator<BaseEventListener> {
    @Override
    public int compare(BaseEventListener o1, BaseEventListener o2) {
        return o1.order() - o2.order();
    }
}
