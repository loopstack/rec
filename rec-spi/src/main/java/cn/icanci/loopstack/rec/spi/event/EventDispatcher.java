package cn.icanci.loopstack.rec.spi.event;

/**
 * 事件分发器抽象
 *
 * @author icanci
 * @since 1.0 Created in 2022/11/11 18:01
 */
public interface EventDispatcher {
    /**
     * 分发事件 同步发送
     *
     * @param event event
     */
    void fire(final BaseEvent event);

    /**
     * 分发事件
     *
     * @param event event
     * @param sync 是否同步发送
     */
    void fire(final BaseEvent event, boolean sync);
}
