package cn.icanci.loopstack.rec.spi.event;

import java.util.EventObject;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 18:01
 */
public class BaseEvent extends EventObject {

    public BaseEvent(Object source) {
        super(source);
    }

    public BaseEvent() {
        super("BaseEvent");
    }
}
