package cn.icanci.loopstack.rec.spi.event;

/**
 * 事件执行
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/11 18:01
 */
public class EventHandler implements Runnable {
    /** 事件 */
    private BaseEvent                    event;

    /** 事件监听器 */
    private BaseEventListener<BaseEvent> listener;

    public EventHandler(BaseEvent event, BaseEventListener<BaseEvent> listener) {
        this.event = event;
        this.listener = listener;
    }

    @Override
    public void run() {
        listener.onEvent(event);
    }
}
