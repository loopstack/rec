package cn.icanci.loopstack.rec.spi.event;

import java.util.EventListener;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 18:01
 */
public abstract class BaseEventListener<T extends BaseEvent> implements EventListener {

    /**
     * 接受Event方法
     * 
     * @param event 事件
     */
    protected abstract void event(T event);

    /**
     * 处理 Event
     * 
     * @param event 事件
     */
    public final void onEvent(T event) {
        if (isSupport(event)) {
            event(event);
        }
    }

    /**
     * 是否支持
     *
     * @param event event
     * @return 返回是否支持
     */
    protected boolean isSupport(T event) {
        return true;
    }

    /**
     * 监听器执行的顺序 数值越小，优先级越高
     *
     * @return 返回顺序
     */
    protected int order() {
        return 0;
    }
}
