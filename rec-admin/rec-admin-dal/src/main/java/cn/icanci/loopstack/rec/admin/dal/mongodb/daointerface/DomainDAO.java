package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DomainDO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 07:05
 */
public interface DomainDAO extends BaseDAO<DomainDO> {
    /**
     * 文档对应的名字
     */
    String          COLLECTION_NAME  = "rec-domain";
    /**
     * 文档对应的Class
     */
    Class<DomainDO> COLLECTION_CLASS = DomainDO.class;

    DomainDO queryByDomainName(String domainName);

    DomainDO queryByDomainCode(String domainCode);

    interface DomainColumn extends BaseColumn {
        /**
         * 域Code
         */
        String domainCode = "domainCode";
        /**
         * 域name
         */
        String domainName = "domainName";
    }
}
