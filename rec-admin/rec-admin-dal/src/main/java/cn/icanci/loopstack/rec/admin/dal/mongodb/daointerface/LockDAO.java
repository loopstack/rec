package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.LockDO;

/**
 * 分布式锁实现参考：https://www.cnblogs.com/xiaoqi/p/mongodb-lock.html
 *
 * @author icanci
 * @since 1.0 Created in 2022/11/26 16:39
 */
public interface LockDAO {
    /**
     * 文档对应的名字
     */
    String        COLLECTION_NAME  = "rec-lock";
    /**
     * 文档对应的Class
     */
    Class<LockDO> COLLECTION_CLASS = LockDO.class;

    String lock(String key, long expireTime);

    boolean release(String key, String token);

    boolean refresh(String key, String token, long expiration);

    interface LockColumn {
        /** id */
        String _id      = "_id";
        /** key */
        String key      = "key";
        /** expireAt */
        String expireAt = "expireAt";
        /** token */
        String token    = "token";
        /** env */
        String env      = "env";
    }
}
