package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.RegisterDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.RegisterDO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 22:00
 */
@Service("registerDAO")
public class MongoRegisterDAO extends AbstractBaseDAO<RegisterDO> implements RegisterDAO {

    @Override
    public void insert(RegisterDO registerDO) {
        super.insert(registerDO);
        mongoTemplate.insert(registerDO, COLLECTION_NAME);
    }

    @Override
    public void update(RegisterDO registerDO) {
        super.update(registerDO);
        mongoTemplate.save(registerDO, COLLECTION_NAME);
    }

    @Override
    public List<RegisterDO> queryAll() {
        Criteria criteria = Criteria.where(RegisterColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<RegisterDO> pageQuery(RegisterDO registerDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(RegisterColumn.env).is(DEFAULT_ENV);
        if (StringUtils.isNotBlank(registerDO.getAppName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(RegisterColumn.appName).regex("^.*" + registerDO.getAppName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(registerDO.getDomain())) {
            criteria.and(RegisterColumn.domain).is(registerDO.getDomain());
        }
        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, RegisterColumn.createTime));
        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public RegisterDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(RegisterColumn._id).is(_id);
        criteria.and(RegisterColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public void deleteById(String id) {
        Criteria criteria = Criteria.where(RegisterColumn._id).is(id);
        criteria.and(RegisterColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        mongoTemplate.remove(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public List<RegisterDO> queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(RegisterColumn.domain).is(domainCode);
        criteria.and(RegisterColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public RegisterDO queryUnionOne(String domain, String clientAddress, int clientPort) {
        Criteria criteria = Criteria.where(RegisterColumn.domain).is(domain);
        criteria.and(RegisterColumn.env).is(DEFAULT_ENV);
        criteria.and(RegisterColumn.clientAddress).is(clientAddress);
        criteria.and(RegisterColumn.clientPort).is(clientPort);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
