package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.SceneDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.SceneDO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 16:04
 */
@Service("sceneDAO")
public class MongoSceneDAO extends AbstractBaseDAO<SceneDO> implements SceneDAO {
    @Override
    public void insert(SceneDO sceneDO) {
        super.insert(sceneDO);
        mongoTemplate.insert(sceneDO, COLLECTION_NAME);
    }

    @Override
    public void update(SceneDO sceneDO) {
        super.update(sceneDO);
        mongoTemplate.save(sceneDO, COLLECTION_NAME);
    }

    @Override
    public List<SceneDO> queryAll() {
        Criteria criteria = Criteria.where(SceneColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);

    }

    @Override
    public PageList<SceneDO> pageQuery(SceneDO sceneDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(SceneColumn.env).is(DEFAULT_ENV);
        if (StringUtils.isNotBlank(sceneDO.getDomainCode())) {
            criteria.and(SceneColumn.domainCode).is(sceneDO.getDomainCode());
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, SceneColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);

    }

    @Override
    public SceneDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(SceneColumn._id).is(_id);
        criteria.and(SceneColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);

    }

    @Override
    public SceneDO queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(SceneColumn.domainCode).is(domainCode);
        criteria.and(SceneColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
