package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DataSourceDO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:04
 */
public interface DataSourceDAO extends BaseDAO<DataSourceDO> {
    /**
     * 文档对应的名字
     */
    String              COLLECTION_NAME  = "rec-datasource";
    /**
     * 文档对应的Class
     */
    Class<DataSourceDO> COLLECTION_CLASS = DataSourceDO.class;

    DataSourceDO queryByDataSourceName(String domainCode, String dataSourceName);

    List<DataSourceDO> queryByDomainCode(String domainCode);

    DataSourceDO queryByUuid(String dataSourceUuid);

    interface DataSourceColumn extends BaseColumn {
        /**
         * 数据源名称
         */
        String dataSourceName = "dataSourceName";
        /**
         * 数据源类型
         */
        String dataSourceType = "dataSourceType";
        /**
         * 数据源DomainCode
         */
        String domainCode     = "domainCode";
    }
}
