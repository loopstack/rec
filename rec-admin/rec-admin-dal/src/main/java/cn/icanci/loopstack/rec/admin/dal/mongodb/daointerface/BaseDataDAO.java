package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import java.util.List;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.BaseDataDO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:02
 */
public interface BaseDataDAO extends BaseDAO<BaseDataDO> {
    /**
     * 文档对应的名字
     */
    String            COLLECTION_NAME  = "rec-basedata";
    /**
     * 文档对应的Class
     */
    Class<BaseDataDO> COLLECTION_CLASS = BaseDataDO.class;

    BaseDataDO queryByFieldNameAndDomainCode(String fieldName, String domainCode);

    List<BaseDataDO> queryByDomainCode(String domainCode);

    BaseDataDO baseDataQueryByUuid(String uuid);

    interface BaseDataColumn extends BaseColumn {
        /**
         * 域Code
         */
        String domainCode    = "domainCode";
        /**
         * 基础数据名称
         */
        String fieldName     = "fieldName";
        /**
         * 数据类型(布尔、字符串、数值、日期、元数据等)
         */
        String dataType      = "dataType";
        /**
         * 脚本执行类型
         */
        String scriptType    = "scriptType";
        /**
         * 脚本内容
         */
        String scriptContent = "scriptContent";
        /**
         * 脚本执行返回类型(只能是基本数据类型)
         */
        @Deprecated
        String resultType    = "resultType";
    }
}
