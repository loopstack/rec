package cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject;

import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ResultTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 11:41
 */
public class BaseDataDO extends BaseDO {
    /**
     * 域Code
     */
    private String domainCode;
    /**
     * 基础数据名称
     */
    private String fieldName;
    /**
     * 数据类型(布尔、字符串、数值、日期、元数据等)
     * 
     * @see DataTypeEnum#name()
     */
    private String dataType;
    /**
     * 关联的元数据uuid
     *
     * {@link DataTypeEnum#METADATA}
     */
    private String metadataUuid;
    /**
     * 脚本执行类型
     *
     * @see ScriptTypeEnum#name() 
     */
    private String scriptType;
    /**
     * 脚本内容
     */
    private String scriptContent;
    /**
     * 脚本执行返回类型(只能是基本数据类型)
     * 
     * @see ResultTypeEnum#name() 
     */
    private String resultType;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getMetadataUuid() {
        return metadataUuid;
    }

    public void setMetadataUuid(String metadataUuid) {
        this.metadataUuid = metadataUuid;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public String getScriptContent() {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
}
