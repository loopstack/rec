package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.StrategyDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.StrategyDO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci(1205068)
 * @version Id: MongoStrategyDAO, v 0.1 2022/10/26 21:15 icanci Exp $
 */
@Service("strategyDAO")
public class MongoStrategyDAO extends AbstractBaseDAO<StrategyDO> implements StrategyDAO {

    @Override
    public void insert(StrategyDO strategyDO) {
        super.insert(strategyDO);
        mongoTemplate.insert(strategyDO, COLLECTION_NAME);
    }

    @Override
    public void update(StrategyDO strategyDO) {
        super.update(strategyDO);
        mongoTemplate.save(strategyDO, COLLECTION_NAME);
    }

    @Override
    public List<StrategyDO> queryAll() {
        Criteria criteria = Criteria.where(StrategyColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<StrategyDO> pageQuery(StrategyDO strategyDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(StrategyColumn.env).is(DEFAULT_ENV);
        if (StringUtils.isNotBlank(strategyDO.getDomainCode())) {
            criteria.and(StrategyColumn.domainCode).is(strategyDO.getDomainCode());
        }
        if (StringUtils.isNotBlank(strategyDO.getStrategyName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(StrategyColumn.strategyName).regex("^.*" + strategyDO.getStrategyName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(strategyDO.getSceneCode())) {
            criteria.and(StrategyColumn.sceneCode).is(strategyDO.getSceneCode());
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, StrategyColumn.createTime));
        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public StrategyDO queryOneById(String _id) {
        // 必须和文档中的字段一致
        Criteria criteria = Criteria.where(StrategyColumn._id).is(_id);
        criteria.and(StrategyColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public StrategyDO queryByStrategyName(String domainCode, String strategyName) {
        Criteria criteria = Criteria.where(StrategyColumn.domainCode).is(domainCode);
        criteria.and(StrategyColumn.env).is(DEFAULT_ENV);
        criteria.and(StrategyColumn.strategyName).is(strategyName);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public List<StrategyDO> queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(StrategyColumn.domainCode).is(domainCode);
        criteria.and(StrategyColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public StrategyDO queryBySceneCode(String domainCode, String sceneCode) {
        Criteria criteria = Criteria.where(StrategyColumn.domainCode).is(domainCode);
        criteria.and(StrategyColumn.env).is(DEFAULT_ENV);
        criteria.and(StrategyColumn.sceneCode).is(sceneCode);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}