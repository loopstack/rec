package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.BaseDataDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.BaseDataDO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:59
 */
@Service("baseDataDAO")
public class MongoBaseDataDAO extends AbstractBaseDAO<BaseDataDO> implements BaseDataDAO {

    @Override
    public void insert(BaseDataDO baseDataDO) {
        super.insert(baseDataDO);
        mongoTemplate.insert(baseDataDO, COLLECTION_NAME);
    }

    @Override
    public void update(BaseDataDO baseDataDO) {
        super.update(baseDataDO);
        mongoTemplate.save(baseDataDO, COLLECTION_NAME);
    }

    @Override
    public List<BaseDataDO> queryAll() {
        Criteria criteria = Criteria.where(BaseDataColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);

    }

    @Override
    public PageList<BaseDataDO> pageQuery(BaseDataDO baseDataDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(BaseDataColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(baseDataDO.getDomainCode())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(BaseDataColumn.domainCode).regex("^.*" + baseDataDO.getDomainCode() + ".*$", "i");
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, BaseDataColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);

    }

    @Override
    public BaseDataDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(BaseDataColumn._id).is(_id);
        criteria.and(BaseDataColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public BaseDataDO queryByFieldNameAndDomainCode(String fieldName, String domainCode) {
        Criteria criteria = Criteria.where(BaseDataColumn.fieldName).is(fieldName);
        criteria.and(BaseDataColumn.env).is(DEFAULT_ENV);
        criteria.and(BaseDataColumn.domainCode).is(domainCode);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public List<BaseDataDO> queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(BaseDataColumn.domainCode).is(domainCode);
        criteria.and(BaseDataColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public BaseDataDO baseDataQueryByUuid(String uuid) {
        Criteria criteria = Criteria.where(BaseDataColumn.uuid).is(uuid);
        criteria.and(BaseDataColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

}
