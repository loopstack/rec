package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.DataSourceDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DataSourceDO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 15:52
 */
@Service("dataSourceDAO")
public class MongoDataSourceDAO extends AbstractBaseDAO<DataSourceDO> implements DataSourceDAO {

    @Override
    public void insert(DataSourceDO dataSourceDO) {
        super.insert(dataSourceDO);
        mongoTemplate.insert(dataSourceDO, COLLECTION_NAME);
    }

    @Override
    public void update(DataSourceDO dataSourceDO) {
        super.update(dataSourceDO);
        mongoTemplate.save(dataSourceDO, COLLECTION_NAME);
    }

    @Override
    public List<DataSourceDO> queryAll() {
        Criteria criteria = Criteria.where(DataSourceColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);

    }

    @Override
    public PageList<DataSourceDO> pageQuery(DataSourceDO dataSourceDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(DataSourceColumn.env).is(DEFAULT_ENV);
        if (StringUtils.isNotBlank(dataSourceDO.getDataSourceName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(DataSourceColumn.dataSourceName).regex("^.*" + dataSourceDO.getDataSourceName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(dataSourceDO.getDataSourceType())) {
            criteria.and(DataSourceColumn.dataSourceType).is(dataSourceDO.getDataSourceType());
        }
        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, DataSourceColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);

    }

    @Override
    public DataSourceDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(DataSourceColumn._id).is(_id);
        criteria.and(DataSourceColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public DataSourceDO queryByDataSourceName(String domainCode, String dataSourceName) {
        Criteria criteria = Criteria.where(DataSourceColumn.dataSourceName).is(dataSourceName);
        criteria.and(DataSourceColumn.env).is(DEFAULT_ENV);
        criteria.and(DataSourceColumn.domainCode).is(domainCode);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public List<DataSourceDO> queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(DataSourceColumn.domainCode).is(domainCode);
        criteria.and(DataSourceColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public DataSourceDO queryByUuid(String dataSourceUuid) {
        Criteria criteria = Criteria.where(DataSourceColumn.uuid).is(dataSourceUuid);
        criteria.and(DataSourceColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
