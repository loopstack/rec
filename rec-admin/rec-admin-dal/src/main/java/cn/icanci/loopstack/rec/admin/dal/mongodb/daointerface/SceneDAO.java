package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.SceneDO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:07
 */
public interface SceneDAO extends BaseDAO<SceneDO> {
    /**
     * 文档对应的名字
     */
    String         COLLECTION_NAME  = "rec-scene";
    /**
     * 文档对应的Class
     */
    Class<SceneDO> COLLECTION_CLASS = SceneDO.class;

    SceneDO queryByDomainCode(String domainCode);

    interface SceneColumn extends BaseColumn {
        /**
         * 域Code
         */
        String domainCode = "domainCode";
    }
}
