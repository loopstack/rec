package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.MetadataDO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:06
 */
public interface MetadataDAO extends BaseDAO<MetadataDO> {
    /**
     * 文档对应的名字
     */
    String            COLLECTION_NAME  = "rec-metadata";
    /**
     * 文档对应的Class
     */
    Class<MetadataDO> COLLECTION_CLASS = MetadataDO.class;

    MetadataDO queryByMetadataName(String metadataName, String domainCode);

    List<MetadataDO> queryByDomainCode(String domainCode);

    MetadataDO queryByUuId(String metadataUuid);

    interface MetadataColumn extends BaseColumn {
        /**
         * 域Code
         */
        String domainCode   = "domainCode";
        /**
         * 元数据名称
         */
        String metadataName = "metadataName";
    }
}
