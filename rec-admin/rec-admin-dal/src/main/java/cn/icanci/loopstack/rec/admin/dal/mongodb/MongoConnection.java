package cn.icanci.loopstack.rec.admin.dal.mongodb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.mongodb.*;

/**
 * 废弃，因为使用了启动器，自动bean注册，无需手动注册bean
 * 
 * @author icanci(1205068)
 * @version Id: MongoConnection, v 0.1 2022/10/29 21:19 icanci Exp $
 */
@Deprecated
public class MongoConnection extends MongoClient {
    /**
     * 创建单片无账号密码客户端
     * 
     * @param host 服务器host
     * @param port 端口
     */
    public MongoConnection(String host, int port) {
        super(host, port);
    }

    /**
     * 创建单片带账号密码客户端
     * 
     * @param userName 连接账号
     * @param password 连接密码
     * @param host host
     * @param port 端口
     * @param databaseName 库名
     */
    public MongoConnection(String userName, String password, String host, int port, String databaseName) {
        super(new ServerAddress(host, port), Collections.singletonList(MongoCredential.createCredential(userName, databaseName, password.toCharArray())),
            MongoClientOptions.builder().readPreference(ReadPreference.primaryPreferred()).build());
    }

    /**
     * 创建服务集带账号密码客户端
     * 
     * @param userName 连接账号
     * @param password 连接密码
     * @param addresses 服务集
     * @param databaseName 库名
     */
    public MongoConnection(String userName, String password, List<ServerAddress> addresses, String databaseName) {
        super(addresses, Collections.singletonList(MongoCredential.createCredential(userName, databaseName, password.toCharArray())),
            MongoClientOptions.builder().readPreference(ReadPreference.primaryPreferred()).build());
    }

    /**
     * 创建服务集带账号密码客户端
     * @param userName 连接账号
     * @param password 连接密码
     * @param addressesStr 服务集地址 字符串 127.0.0.1:27017;127.0.0.1:27018;
     * @param databaseName 库名
     */
    public MongoConnection(String userName, String password, String addressesStr, String databaseName, String readType) {
        super(createServerAddress(addressesStr), Collections.singletonList(MongoCredential.createCredential(userName, databaseName, password.toCharArray())),
            MongoClientOptions.builder().readPreference(readPreference(readType)).build());
    }

    /**
     * readPreference
     * 
     * @param readType readType
     * @return ReadPreference
     */
    private static ReadPreference readPreference(String readType) {
        if (StringUtils.isBlank(readType)) {
            return ReadPreference.secondaryPreferred();
        }
        switch (readType) {
            case "PRIMARY":
                return ReadPreference.primaryPreferred();
            default:
                return ReadPreference.secondaryPreferred();
        }
    }

    /**
     * 拆开服务地址
     * 
     * @param addressStr  127.0.0.1:27017;
     * @return 服务地址
     */
    public static List<ServerAddress> createServerAddress(String addressStr) {
        List<ServerAddress> serverAddressesList = new ArrayList<>();
        String[] addressArray = addressStr.split(";");
        for (String address : addressArray) {
            String[] hostAndPort = address.split(":");
            serverAddressesList.add(new ServerAddress(hostAndPort[0], new Integer(hostAndPort[1])));
        }

        return serverAddressesList;
    }
}