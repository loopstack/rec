package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.StrategyDO;

import java.util.List;

/**
 * 策略
 * 
 * @author icanci(1205068)
 * @version Id: StrategyDAO, v 0.1 2022/10/26 21:15 icanci Exp $
 */
public interface StrategyDAO extends BaseDAO<StrategyDO> {
    /**
     * 文档对应的名字
     */
    String            COLLECTION_NAME  = "rec-strategy";
    /**
     * 文档对应的Class
     */
    Class<StrategyDO> COLLECTION_CLASS = StrategyDO.class;

    StrategyDO queryByStrategyName(String domainCode, String strategyName);

    List<StrategyDO> queryByDomainCode(String domainCode);

    StrategyDO queryBySceneCode(String domainCode, String sceneCode);

    interface StrategyColumn extends BaseColumn {
        /**
         * 域Code
         */
        String domainCode     = "domainCode";
        /**
         * 场景Code
         */
        String sceneCode      = "sceneCode";
        /**
         * 策略组名称
         */
        String strategyName   = "strategyName";
        /**
         * 数据源类型(脚本、接口、SQL)
         */
        String dataSourceType = "dataSourceType";
        /**
         * 数据源关联uuid
         */
        String dataSourceUuid = "dataSourceUuid";
        /**
         * 规则配置类型(默认为List)
         */
        String ruleType       = "ruleType";
    }
}