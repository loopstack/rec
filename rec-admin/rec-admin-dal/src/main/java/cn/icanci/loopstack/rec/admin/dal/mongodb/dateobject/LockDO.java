package cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject;

import org.springframework.data.annotation.Id;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 16:39
 */
public class LockDO {
    @Id
    private String id;
    private long   expireAt;
    private String token;
    private String key;
    private String env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(long expireAt) {
        this.expireAt = expireAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
