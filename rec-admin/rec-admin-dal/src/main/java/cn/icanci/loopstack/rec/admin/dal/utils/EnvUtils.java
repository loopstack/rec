package cn.icanci.loopstack.rec.admin.dal.utils;

import cn.icanci.loopstack.rec.admin.dal.utils.service.impl.EnvServiceImpl;
import cn.icanci.loopstack.rec.admin.dal.utils.service.EnvService;

import org.apache.commons.lang3.StringUtils;

/**
 * 环境标识
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:26
 */
public class EnvUtils {

    private static final String DEFAULT_ENV = "test";

    private static EnvService   envService;

    private static String       currEnv;

    public static String getEnv() {
        if (StringUtils.isBlank(currEnv)) {
            String env = envService.getEnv();
            currEnv = StringUtils.isBlank(env) ? DEFAULT_ENV : env;
        }
        return currEnv;
    }

    public static void setEnvService(EnvServiceImpl envService) {
        EnvUtils.envService = envService;
    }
}
