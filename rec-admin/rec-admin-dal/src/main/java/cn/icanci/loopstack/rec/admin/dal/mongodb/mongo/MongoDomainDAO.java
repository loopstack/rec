package cn.icanci.loopstack.rec.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.DomainDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DomainDO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 15:15
 */
@Service("domainDAO")
public class MongoDomainDAO extends AbstractBaseDAO<DomainDO> implements DomainDAO {

    @Override
    public void insert(DomainDO domainDO) {
        super.insert(domainDO);
        mongoTemplate.insert(domainDO, COLLECTION_NAME);
    }

    @Override
    public void update(DomainDO domainDO) {
        super.update(domainDO);
        mongoTemplate.save(domainDO, COLLECTION_NAME);
    }

    @Override
    public List<DomainDO> queryAll() {
        Criteria criteria = Criteria.where(DomainColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<DomainDO> pageQuery(DomainDO domainDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(DomainColumn.env).is(DEFAULT_ENV);
        if (StringUtils.isNotBlank(domainDO.getDomainName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(DomainColumn.domainName).regex("^.*" + domainDO.getDomainName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(domainDO.getDomainCode())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(DomainColumn.domainCode).regex("^.*" + domainDO.getDomainCode() + ".*$", "i");
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, DomainColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public DomainDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(DomainColumn._id).is(_id);
        criteria.and(DomainColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public DomainDO queryByDomainName(String domainName) {
        Criteria criteria = Criteria.where(DomainColumn.domainName).is(domainName);
        criteria.and(DomainColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public DomainDO queryByDomainCode(String domainCode) {
        Criteria criteria = Criteria.where(DomainColumn.domainCode).is(domainCode);
        criteria.and(DomainColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
