package cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.RegisterDO;

import java.util.List;

/**
 * 注册中心
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/17 19:02
 */
public interface RegisterDAO extends BaseDAO<RegisterDO> {
    /**
     * 文档对应的名字
     */
    String            COLLECTION_NAME  = "rec-register";
    /**
     * 文档对应的Class
     */
    Class<RegisterDO> COLLECTION_CLASS = RegisterDO.class;

    void deleteById(String id);

    List<RegisterDO> queryByDomainCode(String domainCode);

    RegisterDO queryUnionOne(String domain, String clientAddress, int clientPort);

    interface RegisterColumn extends BaseColumn {
        String clientAddress  = "clientAddress";
        String clientPort     = "clientPort";
        String appName        = "appName";
        String registerTime   = "registerTime";
        String lastUpdateTime = "lastUpdateTime";
        String domain         = "domain";
    }
}
