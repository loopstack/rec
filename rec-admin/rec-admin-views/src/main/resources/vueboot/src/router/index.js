import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '../views/layout/Layout'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/404'),
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        hidden: false,
        children: [{
            path: 'dashboard',
            name: 'dashboard',
            meta: {
                title: 'REC-规则引擎',
                icon: 'dashboard'
            },
            component: () => import('@/views/dashboard/index')
        }]
    },
    {
        path: '/ruleConfig',
        component: Layout,
        meta: {
            title: '规则配置',
            icon: 'example'
        },
        children: [
            {
                path: 'domain',
                name: 'domain',
                component: () => import('@/views/ruleConfig/domain'),
                meta: {
                    title: '域配置',
                    icon: 'edit'
                }
            }, {
                path: 'metadata',
                name: 'metadata',
                component: () => import('@/views/ruleConfig/metadata'),
                meta: {
                    title: '元数据配置',
                    icon: 'exit-fullscreen'
                }
            }, {
                path: 'baseData',
                name: 'baseData',
                component: () => import('@/views/ruleConfig/baseData'),
                meta: {
                    title: '基础数据配置',
                    icon: 'form'
                }
            }, {
                path: 'dataSource',
                name: 'dataSource',
                component: () => import('@/views/ruleConfig/dataSource'),
                meta: {
                    title: '数据源配置',
                    icon: 'guide'
                }
            }, {
                path: 'strategy',
                name: 'strategy',
                component: () => import('@/views/ruleConfig/strategy'),
                meta: {
                    title: '策略配置',
                    icon: 'link'
                }
            },
        ]
    },
    // {
    //     path: '/recLogs',
    //     component: Layout,
    //     meta: {
    //         title: '执行日志',
    //         icon: 'tab'
    //     },
    //     children: [
    //         {
    //             path: 'log',
    //             name: 'log',
    //             component: () => import('@/views/recLogs/log'),
    //             meta: {
    //                 title: '执行日志',
    //                 icon: 'clipboard'
    //             }
    //         },
    //         {
    //             path: 'logDetail',
    //             name: 'logDetail',
    //             component: () => import('@/views/recLogs/logDetail'),
    //             meta: {
    //                 title: '执行日志详情',
    //                 icon: 'clipboard'
    //             }
    //         },
    //     ]
    // },
    {
        path: '/registerCenter',
        component: Layout,
        meta: {
            title: '注册中心',
            icon: 'education'
        },
        children: [
            {
                path: 'register',
                name: 'register',
                component: () => import('@/views/register/register'),
                meta: {
                    title: '注册中心',
                    icon: 'bug'
                }
            },
        ]
    },
    {
        path: '/doc',
        component: Layout,
        meta: {
            title: '开放文档',
            icon: 'tab'
        },
        children: [
            {
                path: 'docDetail',
                name: 'docDetail',
                component: () => import('@/views/doc/docDetail'),
                meta: {
                    title: '项目文档',
                    icon: 'clipboard'
                }
            },
        ]
    },
    {
        path: '*',
        redirect: '/404',
        hidden: true
    }
]

export default new Router({
    // mode: 'history', //后端支持可开
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRouterMap
})
