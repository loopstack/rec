import request from '@/utils/request'

/**
 * 验证 dataSourceName
 *
 * @param dataSourceName dataSourceName
 * @returns {*}
 */
export async function remoteValidateDataSourceName(domainCode, dataSourceName) {
  return await request({
    url: '/rec/dataSource/validateDataSourceName/' + domainCode + "/" + dataSourceName,
    method: 'get',
  })
}

/**
 * 保存
 *
 * @param dataSource dataSource
 * @returns {*}
 */
export async function saveDataSource(dataSource) {
  return await request({
    url: '/rec/dataSource/save',
    method: 'post',
    data: dataSource
  })
}

/**
 * 分页查询
 *
 * @param dataSource dataSource
 * @param paginator paginator
 * @returns {*}
 */
export async function dataSourcePageQuery(dataSource, paginator) {
  return await request({
    url: '/rec/dataSource/query',
    method: 'post',
    data: {
      'dataSource': dataSource,
      'paginator': paginator
    }
  })
}

/**
 * 测试
 *
 * @param dataSource dataSource
 * @returns {*}
 */
export async function dataSourceDebug(dataSource) {
  return await request({
    url: '/rec/dataSource/debug',
    method: 'post',
    data: dataSource
  })
}

/**
 * 测试
 *
 * @param dataSourceUuid dataSourceUuid
 * @returns {*}
 */
export async function dataSourceDebugForUuid(dataSourceUuid) {
  return await request({
    url: '/rec/dataSource/debugUuid/' + dataSourceUuid,
    method: 'get',
  })
}
