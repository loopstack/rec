import request from '@/utils/request'

/**
 * 验证 domainName
 *
 * @param domainName domainName
 * @returns {*}
 */
export async function remoteValidateDomainName(domainName) {
  return await request({
    url: '/rec/domain/validateDomainName/' + domainName,
    method: 'get',
  })
}

/**
 * 验证 domainCode
 *
 * @param domainCode domainCode
 * @returns {*}
 */
export async function remoteValidateDomainCode(domainCode) {
  return await request({
    url: '/rec/domain/validateDomainCode/' + domainCode,
    method: 'get',
  })
}

/**
 * 保存域
 *
 * @param domain domain
 * @returns {*}
 */
export async function saveDomain(domain) {
  return await request({
    url: '/rec/domain/save',
    method: 'post',
    data: domain
  })
}


/**
 * 分页查询
 *
 * @param domain domain
 * @param paginator paginator
 * @returns {*}
 */
export async function domainPageQuery(domain, paginator) {
  return await request({
    url: '/rec/domain/query',
    method: 'post',
    data: {
      'domain': domain,
      'paginator': paginator
    }
  })
}
