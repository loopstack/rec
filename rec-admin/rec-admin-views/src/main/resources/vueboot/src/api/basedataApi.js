import request from '@/utils/request'

/**
 * 验证 fieldName
 *
 * @param fieldName fieldName
 * @param domainCode domainCode
 * @returns {*}
 */
export async function remoteValidateFieldName(fieldName, domainCode) {
    return await request({
        url: '/rec/baseData/validateFieldName/' + fieldName + "/" + domainCode,
        method: 'get',
    })
}

/**
 * 保存
 *
 * @param baseData baseData
 * @returns {*}
 */
export async function saveBaseData(baseData) {
    return await request({
        url: '/rec/baseData/save',
        method: 'post',
        data: baseData
    })
}

/**
 * 分页查询
 *
 * @param baseData baseData
 * @param paginator paginator
 * @returns {*}
 */
export async function baseDataPageQuery(baseData, paginator) {
    return await request({
        url: '/rec/baseData/query',
        method: 'post',
        data: {
            'baseData': baseData,
            'paginator': paginator
        }
    })
}

/**
 * 测试
 *
 * @param baseData baseData
 * @param scriptContentTest scriptContentTest
 * @returns {*}
 */
export async function baseDataDebug(baseData, scriptContentTest) {
    return await request({
        url: '/rec/baseData/debug',
        method: 'post',
        data: {
            'baseData': baseData,
            'scriptContentTest': scriptContentTest
        }
    })
}

/**
 * baseDataQueryByUuid
 *
 * @param uuid uuid
 * @returns {*}
 */
export async function baseDataQueryByUuid(uuid) {
    return await request({
        url: '/rec/baseData/baseDataQueryByUuid/' + uuid,
        method: 'get',
    })
}
