import request from '@/utils/request'

/**
 * 验证 metadataName
 *
 * @param metadataName metadataName
 * @param domainCode domainCode
 * @returns {*}
 */
export async function remoteValidateMetadataName(metadataName, domainCode) {
  return await request({
    url: '/rec/metadata/validateMetadataName/' + metadataName + "/" + domainCode,
    method: 'get',
  })
}

/**
 * 保存
 *
 * @param metadata metadata
 * @returns {*}
 */
export async function saveMetadata(metadata) {
  return await request({
    url: '/rec/metadata/save',
    method: 'post',
    data: metadata
  })
}


/**
 * 分页查询
 *
 * @param metadata metadata
 * @param paginator paginator
 * @returns {*}
 */
export async function metadataPageQuery(metadata, paginator) {
  return await request({
    url: '/rec/metadata/query',
    method: 'post',
    data: {
      'metadata': metadata,
      'paginator': paginator
    }
  })
}
