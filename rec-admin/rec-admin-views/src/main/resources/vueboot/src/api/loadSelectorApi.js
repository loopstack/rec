import request from '@/utils/request'

/**
 * loadDomainSelector
 *
 * @returns {*}
 */
export async function loadDomainSelector() {
  let ret = await request({
    url: '/rec/domain/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadBaseDataSelector
 *
 * @returns {*}
 */
export async function loadBaseDataSelector(domainCode) {
  let ret = await request({
    url: '/rec/baseData/loadSelector/' + domainCode,
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}


/**
 * @param domainCode domainCode
 * @returns {*}
 */
export async function loadDomainSceneSelector(domainCode) {
  let ret = await request({
    url: '/rec/scene/loadSelector/' + domainCode,
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * @param domainCode domainCode
 * @returns {*}
 */
export async function loadDomainMetadataSelector(domainCode) {
  let ret = await request({
    url: '/rec/metadata/loadSelector/' + domainCode,
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * @param domainCode domainCode
 * @returns {*}
 */
export async function loadDataSourceSelector(domainCode) {
  let ret = await request({
    url: '/rec/dataSource/loadSelector/' + domainCode,
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadDataSourceTypeSelector
 *
 * @returns {*}
 */
export async function loadDataSourceTypeSelector() {
  let ret = await request({
    url: '/rec/common/dataSourceType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadDataTypeSelector
 *
 * @returns {*}
 */
export async function loadDataTypeSelector() {
  let ret = await request({
    url: '/rec/common/dataType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadHttpRequestTypeSelector
 *
 * @returns {*}
 */
export async function loadHttpRequestTypeSelector() {
  let ret = await request({
    url: '/rec/common/httpRequestType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadInterruptSelector
 *
 * @returns {*}
 */
export async function loadInterruptSelector() {
  let ret = await request({
    url: '/rec/common/interrupt',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadOperatorSelector
 *
 * @returns {*}
 */
export async function loadOperatorSelector() {
  let ret = await request({
    url: '/rec/common/operator',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadResultTypeSelector
 *
 * @returns {*}
 */
export async function loadResultTypeSelector() {
  let ret = await request({
    url: '/rec/common/resultType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadRuleTypeSelector
 *
 * @returns {*}
 */
export async function loadRuleTypeSelector() {
  let ret = await request({
    url: '/rec/common/ruleType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadRuleModeSelector
 *
 * @returns {*}
 */
export async function loadRuleModeSelector() {
  let ret = await request({
    url: '/rec/common/ruleMode',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadScriptTypeSelector
 *
 * @returns {*}
 */
export async function loadScriptTypeSelector() {
  let ret = await request({
    url: '/rec/common/scriptType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}
