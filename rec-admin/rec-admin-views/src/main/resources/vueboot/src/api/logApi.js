import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param domain domain
 * @param paginator paginator
 * @returns {*}
 */
export async function logPageQuery(logOperate, paginator) {
  return await request({
    url: '/rec/log/query',
    method: 'post',
    data: {
      'logOperate': logOperate,
      'paginator': paginator
    }
  })
}
