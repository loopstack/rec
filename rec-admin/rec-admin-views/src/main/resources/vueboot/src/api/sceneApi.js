import request from '@/utils/request'

/**
 * 保存scene
 *
 * @param scene scene
 * @returns {*}
 */
export async function saveScene(scene) {
  return await request({
    url: '/rec/scene/save',
    method: 'post',
    data: scene
  })
}

/**
 * 查询场景信息
 *
 * @param domainCode domainCode
 * @returns {*}
 */
export async function sceneQuery(domainCode) {
  return await request({
    url: '/rec/scene/queryByDomainCode/' + domainCode,
    method: 'get',
  })
}
