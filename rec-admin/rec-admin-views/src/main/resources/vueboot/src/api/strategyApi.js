import request from '@/utils/request'

/**
 * 验证 strategyName
 *
 * @param domainCode domainCode
 * @param dataSourceName dataSourceName
 * @returns {*}
 */
export async function remoteValidateStrategyName(domainCode, dataSourceName) {
  return await request({
    url: '/rec/strategy/validateStrategyName/' + domainCode + "/" + dataSourceName,
    method: 'get',
  })
}

/**
 * 验证 remoteValidateSceneCode
 *
 * @param domainCode domainCode
 * @param sceneCode sceneCode
 * @returns {*}
 */
export async function remoteValidateSceneCode(domainCode, sceneCode) {
  return await request({
    url: '/rec/strategy/validateSceneCode/' + domainCode + "/" + sceneCode,
    method: 'get',
  })
}

/**
 * 保存
 *
 * @param strategy strategy
 * @returns {*}
 */
export async function saveStrategy(strategy) {
  return await request({
    url: '/rec/strategy/save',
    method: 'post',
    data: strategy
  })
}

/**
 * 分页查询
 *
 * @param strategy strategy
 * @param paginator paginator
 * @returns {*}
 */
export async function strategyPageQuery(strategy, paginator) {
  return await request({
    url: '/rec/strategy/query',
    method: 'post',
    data: {
      'strategy': strategy,
      'paginator': paginator
    }
  })
}

/**
 * 测试
 *
 * @param strategy strategy
 * @param scriptContentTest scriptContentTest
 * @returns {*}
 */
export async function strategyDebug(strategy,scriptContentTest) {
  return await request({
    url: '/rec/strategy/debug',
    method: 'post',
    data: {
      'strategy':strategy,
      'scriptContentTest':scriptContentTest
    }
  })
}


/**
 * 获取Boolean操作类型的选择框
 */
export function getBooleanOperator() {
  return [
    {label: '等于(=)', value: 'EQ'},
    {label: '不等于(!=)', value: 'NE'},
  ]
}

/**
 * 获取number操作类型的选择框
 */
export function getNumberOperator() {
  return [
    {label: '等于(=)', value: 'EQ'},
    {label: '大于(>)', value: 'GT'},
    {label: '大于等于(>=)', value: 'GTE'},
    {label: '小于(<)', value: 'LT'},
    {label: '小于等于(<=)', value: 'LTE'},
    {label: '不等于(!=)', value: 'NE'},
  ]
}

/**
 * 获取String操作类型的选择框
 */
export function getStringOperator() {
  return [
    {label: '等于(=)', value: 'EQ'},
    {label: '不等于(!=)', value: 'NE'},
    {label: '包含(⊇)', value: 'CONTAIN'},
    {label: '不包含', value: 'UN_CONTAIN'},
    // {label: '包含于(⊆或⊂)', value: 'INCLUDED'},
  ]
}

/**
 * 获取Date操作类型的选择框
 */
export function getDateOperator() {
  return [
    {label: '等于(=)', value: 'EQ'},
    {label: '大于(>)', value: 'GT'},
    {label: '大于等于(>=)', value: 'GTE'},
    {label: '小于(<)', value: 'LT'},
    {label: '小于等于(<=)', value: 'LTE'},
    {label: '不等于(!=)', value: 'NE'},
  ]
}

/**
 * 获取Metadata操作类型的选择框
 */
export function getMetadataOperator() {
  return [
    {label: '等于(=)', value: 'EQ'},
    {label: '不等于(!=)', value: 'NE'},
    {label: '包含(⊇)', value: 'CONTAIN'},
    {label: '不包含', value: 'UN_CONTAIN'},
    {label: '包含于(⊆或⊂)', value: 'INCLUDED'},
  ]
}

/**
 * 获取布尔操作下拉框
 * @returns {[{label: string, value: string}, {label: string, value: string}]}
 */
export function getBooleanValueOptions() {
  return [
    {label: 'TRUE', value: 'TRUE'},
    {label: 'FALSE', value: 'FALSE'},
  ]
}
