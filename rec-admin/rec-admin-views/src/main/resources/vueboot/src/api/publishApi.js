import request from '@/utils/request'

/**
 * 发布
 *
 * @param domainCode domainCode
 * @returns {Promise<*>}
 */
export async function domainPublish(uuid,domainCode) {
    return await request({
        url: '/rec/register/publish',
        method: 'post',
        data: {
            'domainCode': domainCode,
            'uuid': uuid,
        }
    })
}
