import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param register register
 * @param paginator paginator
 * @returns {*}
 */
export async function registerPageQuery(register, paginator) {
    return await request({
        url: '/rec/register/query',
        method: 'post',
        data: {
            'register': register,
            'paginator': paginator
        }
    })
}
