import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/rec/user/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/rec/user/info/' + token,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/rec/user/logout',
    method: 'post'
  })
}
