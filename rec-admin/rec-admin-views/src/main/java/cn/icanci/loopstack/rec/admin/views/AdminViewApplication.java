package cn.icanci.loopstack.rec.admin.views;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author icanci
 * @since 1.0 Created in 2022/07/08 22:13
 */
@ComponentScan(basePackages = { "cn.icanci.loopstack.rec" })
@SpringBootApplication
public class AdminViewApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminViewApplication.class, args);
    }
}
