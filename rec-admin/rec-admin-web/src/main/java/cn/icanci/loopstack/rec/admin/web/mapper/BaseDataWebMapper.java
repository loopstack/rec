package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ResultTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.web.model.BaseData;
import cn.icanci.loopstack.rec.common.model.config.BaseDataVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:19
 */
@Mapper(componentModel = "spring", uses = { DataTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            ResultTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface BaseDataWebMapper extends BaseWebMapper<BaseData, BaseDataVO> {
}
