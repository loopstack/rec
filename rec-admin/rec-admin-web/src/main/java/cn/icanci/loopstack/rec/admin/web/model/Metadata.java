package cn.icanci.loopstack.rec.admin.web.model;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 13:33
 */
public class Metadata extends Base {
    private static final long  serialVersionUID = -8006929115092670576L;
    /**
     * 域Code
     */
    private String             domainCode;
    /**
     * 元数据名称
     */
    private String             metadataName;
    /**
     * 元数据对
     */
    private List<MetadataPair> metadataPairs;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public List<MetadataPair> getMetadataPairs() {
        return metadataPairs;
    }

    public void setMetadataPairs(List<MetadataPair> metadataPairs) {
        this.metadataPairs = metadataPairs;
    }

    /**
     * 元数据对
     */
    public static class MetadataPair {
        /**
         * key
         */
        private String key;
        /**
         * value
         */
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
