package cn.icanci.loopstack.rec.admin.web.controller.config;

import cn.icanci.loopstack.rec.admin.biz.service.SceneService;
import cn.icanci.loopstack.rec.admin.web.mapper.SceneWebMapper;
import cn.icanci.loopstack.rec.admin.web.model.Scene;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.result.R;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 16:53
 */
@RestController
@RequestMapping("/rec/scene")
public class SceneController {
    @Resource
    private SceneService   sceneService;
    @Resource
    private SceneWebMapper sceneWebMapper;

    // ================================ CRUD ================================

    @GetMapping("queryByDomainCode/{domainCode}")
    public R query(@PathVariable("domainCode") String domainCode) {
        return R.builderOk().data("scene", sceneWebMapper.vo2web(sceneService.queryByDomainCode(domainCode))).build();
    }

    @PostMapping("save")
    public R save(@RequestBody Scene scene) {
        sceneService.save(sceneWebMapper.web2vo(scene));
        return R.builderOk().build();
    }

    // ================================ Load Selectors ================================

    @GetMapping("loadSelector/{domainCode}")
    public R loadSelector(@PathVariable("domainCode") String domainCode) {
        List<TextValue> textValues = sceneService.loadSelector(domainCode);
        return R.builderOk().data("textValues", textValues).build();
    }
}
