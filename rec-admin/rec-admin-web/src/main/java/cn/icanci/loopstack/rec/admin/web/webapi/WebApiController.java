package cn.icanci.loopstack.rec.admin.web.webapi;

import cn.icanci.loopstack.rec.common.aggregation.model.*;
import cn.icanci.loopstack.rec.admin.biz.service.WebApiService;
import cn.icanci.loopstack.rec.common.aggregation.WebApiRequest;
import cn.icanci.loopstack.rec.common.aggregation.WebApiResponse;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * 获取有效的
 *
 * @author icanci
 * @since 1.0 Created in 2022/11/15 21:51
 */
@RestController
@RequestMapping("/rec/webapi")
public class WebApiController {

    @Resource
    private WebApiService webApiService;

    @RequestMapping("/loadDomains")
    public WebApiResponse<DomainDTO> loadDomains(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadDomainByDomains(request.getDomainCodes()));
    }

    @RequestMapping("/loadDomainCodes")
    public WebApiResponse<String> loadDomainCodes() {
        return new WebApiResponse<>(Lists.newArrayList(webApiService.loadAllDomainCodes()));
    }

    @RequestMapping("/loadScenes")
    public WebApiResponse<SceneDTO> loadScenes(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadSceneByDomains(request.getDomainCodes()));
    }

    @RequestMapping("/loadMetadatas")
    public WebApiResponse<MetadataDTO> loadMetadatas(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadMetadataByDomains(request.getDomainCodes()));
    }

    @RequestMapping("/loadBaseDatas")
    public WebApiResponse<BaseDataDTO> loadBaseDatas(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadBaseDataByDomains(request.getDomainCodes()));
    }

    @RequestMapping("/loadDataSources")
    public WebApiResponse<DataSourceDTO> loadDataSources(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadDataSourceByDomains(request.getDomainCodes()));
    }

    @RequestMapping("/loadStrategies")
    public WebApiResponse<StrategyDTO> loadStrategies(@RequestBody WebApiRequest request) {
        return new WebApiResponse<>(webApiService.loadStrategyByDomains(request.getDomainCodes()));
    }
}
