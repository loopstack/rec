package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.web.model.Scene;
import cn.icanci.loopstack.rec.common.model.config.SceneVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:43
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface SceneWebMapper extends BaseWebMapper<Scene, SceneVO> {

    SceneVO.ScenePair web2vo(Scene.ScenePair scenePair);

    Scene.ScenePair vo2web(SceneVO.ScenePair scenePair);
}
