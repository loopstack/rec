package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.common.model.config.RegisterVO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 14:17
 */
public class RegisterQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = 5299992907436244989L;

    private RegisterVO        register;

    public RegisterVO getRegister() {
        return register;
    }

    public void setRegister(RegisterVO register) {
        this.register = register;
    }
}
