package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.MetadataDO;
import cn.icanci.loopstack.rec.admin.web.model.Metadata;
import cn.icanci.loopstack.rec.common.model.config.MetadataVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:40
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface MetadataWebMapper extends BaseWebMapper<Metadata, MetadataVO> {
    MetadataVO.MetadataPair web2vo(MetadataDO.MetadataPair metadataPair);

    Metadata.MetadataPair vo2web(MetadataVO.MetadataPair metadataPair);
}
