package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.Domain;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 11:24
 */
public class DomainQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = 2911663907403903086L;

    private Domain            domain;

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }
}
