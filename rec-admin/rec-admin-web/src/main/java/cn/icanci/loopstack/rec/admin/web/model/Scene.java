package cn.icanci.loopstack.rec.admin.web.model;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 13:34
 */
public class Scene extends Base {
    private static final long serialVersionUID = -8373561318951746275L;
    /**
     * 域Code
     */
    private String            domainCode;
    /**
     * 场景对
     */
    private List<ScenePair>   scenePairs;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public List<ScenePair> getScenePairs() {
        return scenePairs;
    }

    public void setScenePairs(List<ScenePair> scenePairs) {
        this.scenePairs = scenePairs;
    }

    /**
     * 场景对
     */
    public static class ScenePair {
        /**
         * 场景名称
         */
        private String sceneName;
        /**
         * 场景Code
         */
        private String sceneCode;

        public String getSceneName() {
            return sceneName;
        }

        public void setSceneName(String sceneName) {
            this.sceneName = sceneName;
        }

        public String getSceneCode() {
            return sceneCode;
        }

        public void setSceneCode(String sceneCode) {
            this.sceneCode = sceneCode;
        }
    }
}
