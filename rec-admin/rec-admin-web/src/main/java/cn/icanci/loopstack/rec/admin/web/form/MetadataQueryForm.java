package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.Metadata;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 18:04
 */
public class MetadataQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = 996954177624835701L;

    private Metadata          metadata;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
