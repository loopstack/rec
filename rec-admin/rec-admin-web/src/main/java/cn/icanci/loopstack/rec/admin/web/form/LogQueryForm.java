package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.LogOperate;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:25
 */
public class LogQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = -934696142423210674L;

    private LogOperate        logOperate;

    public LogOperate getLogOperate() {
        return logOperate;
    }

    public void setLogOperate(LogOperate logOperate) {
        this.logOperate = logOperate;
    }

}
