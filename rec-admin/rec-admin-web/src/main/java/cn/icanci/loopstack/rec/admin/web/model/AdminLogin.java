package cn.icanci.loopstack.rec.admin.web.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录请求
 * 
 * @author icanci
 * @since 1.0 Created in 2022/04/25 16:42
 */
public class AdminLogin implements Serializable {
    private static final long serialVersionUID = -6109731549811158442L;
    /**
     * 主键
     */
    private Integer           id;
    /**
     * 用户名
     */
    private String            username;
    /**
     * 密码
     */
    private String            password;
    /**
     * 权限
     */
    private Integer           security;
    /**
     * 创建时间
     */
    private Date              createTime;
    /**
     * 角色
     */
    private String            roles;
    /**
     * 头像地址
     */
    private String            avatar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSecurity() {
        return security;
    }

    public void setSecurity(Integer security) {
        this.security = security;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
