package cn.icanci.loopstack.rec.admin.web.controller.config;

import cn.icanci.loopstack.rec.admin.biz.model.StrategyDebugResult;
import cn.icanci.loopstack.rec.admin.biz.service.StrategyService;
import cn.icanci.loopstack.rec.admin.web.form.StrategyDebugForm;
import cn.icanci.loopstack.rec.admin.web.form.StrategyQueryForm;
import cn.icanci.loopstack.rec.admin.web.mapper.StrategyWebMapper;
import cn.icanci.loopstack.rec.admin.web.model.Strategy;
import cn.icanci.loopstack.rec.common.model.config.StrategyVO;
import cn.icanci.loopstack.rec.common.result.R;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:08
 */
@RestController
@RequestMapping("/rec/strategy")
public class StrategyController {
    @Resource
    private StrategyService   strategyService;
    @Resource
    private StrategyWebMapper strategyWebMapper;

    // ================================ CRUD ================================
    @PostMapping("query")
    public R query(@RequestBody StrategyQueryForm form) {
        return R.builderOk()
            .data("queryPage", strategyService.queryPage(strategyWebMapper.web2vo(form.getStrategy()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize()))
            .build();
    }

    @PostMapping("save")
    public R save(@RequestBody Strategy strategy) {
        strategyService.save(strategyWebMapper.web2vo(strategy));
        return R.builderOk().build();
    }

    @GetMapping("validateStrategyName/{domainCode}/{strategyName}")
    public R validateStrategyName(@PathVariable("domainCode") String domainCode, @PathVariable("strategyName") String strategyName) {
        StrategyVO strategy = strategyService.queryByStrategyName(domainCode, strategyName);
        return R.builderOk().data("result", strategy == null).build();
    }

    @GetMapping("validateSceneCode/{domainCode}/{sceneCode}")
    public R validateSceneCode(@PathVariable("domainCode") String domainCode, @PathVariable("sceneCode") String sceneCode) {
        StrategyVO strategy = strategyService.queryBySceneCode(domainCode, sceneCode);
        return R.builderOk().data("result", strategy == null).build();
    }
    // ================================ Debug ================================

    @PostMapping("debug")
    public R debug(@RequestBody StrategyDebugForm strategyDebug) {
        StrategyDebugResult result = strategyService.debug(strategyWebMapper.web2vo(strategyDebug.getStrategy()), strategyDebug.getScriptContentTest());
        return R.builderOk().data("result", result).build();
    }
}
