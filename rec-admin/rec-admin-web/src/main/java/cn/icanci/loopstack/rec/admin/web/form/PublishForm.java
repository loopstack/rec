package cn.icanci.loopstack.rec.admin.web.form;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/20 19:54
 */
public class PublishForm implements Serializable {
    private static final long serialVersionUID = -3531200558118766368L;

    /**
     * 发布的域
     */
    private String            domainCode;
    /**
     * 发布的域uuid
     */
    private String            uuid;

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
