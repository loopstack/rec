package cn.icanci.loopstack.rec.admin.web.controller.config;

import cn.icanci.loopstack.rec.admin.biz.service.MetadataService;
import cn.icanci.loopstack.rec.admin.web.mapper.MetadataWebMapper;
import cn.icanci.loopstack.rec.admin.web.form.MetadataQueryForm;
import cn.icanci.loopstack.rec.admin.web.model.Metadata;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.MetadataVO;
import cn.icanci.loopstack.rec.common.result.R;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 18:03
 */
@RestController
@RequestMapping("/rec/metadata")
public class MetadataController {
    @Resource
    private MetadataService   metadataService;
    @Resource
    private MetadataWebMapper metadataWebMapper;

    // ================================ CRUD ================================
    @PostMapping("query")
    public R query(@RequestBody MetadataQueryForm form) {
        return R.builderOk()
            .data("queryPage", metadataService.queryPage(metadataWebMapper.web2vo(form.getMetadata()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize()))
            .build();
    }

    @PostMapping("save")
    public R save(@RequestBody Metadata metadata) {
        metadataService.save(metadataWebMapper.web2vo(metadata));
        return R.builderOk().build();
    }

    @GetMapping("validateMetadataName/{metadataName}/{domainCode}")
    public R save(@PathVariable("metadataName") String metadataName, @PathVariable("domainCode") String domainCode) {
        MetadataVO metadata = metadataService.queryByMetadataName(metadataName, domainCode);
        return R.builderOk().data("result", metadata == null).build();
    }

    // ================================ Load Selectors ================================

    @GetMapping("loadSelector/{domainCode}")
    public R loadSelector(@PathVariable("domainCode") String domainCode) {
        List<TextValue> textValues = metadataService.loadSelector(domainCode);
        return R.builderOk().data("textValues", textValues).build();
    }
}
