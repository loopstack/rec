package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.Strategy;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:09
 */
public class StrategyDebugForm implements Serializable {
    private static final long serialVersionUID = 7046936486304373642L;

    private Strategy strategy;

    private String            scriptContentTest;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public String getScriptContentTest() {
        return scriptContentTest;
    }

    public void setScriptContentTest(String scriptContentTest) {
        this.scriptContentTest = scriptContentTest;
    }
}
