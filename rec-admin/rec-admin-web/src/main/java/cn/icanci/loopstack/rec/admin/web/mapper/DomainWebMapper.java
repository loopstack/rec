package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.web.model.Domain;
import cn.icanci.loopstack.rec.common.model.config.DomainVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 21:16
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DomainWebMapper extends BaseWebMapper<Domain, DomainVO> {
}
