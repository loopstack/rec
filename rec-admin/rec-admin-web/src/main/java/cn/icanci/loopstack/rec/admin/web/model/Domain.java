package cn.icanci.loopstack.rec.admin.web.model;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 15:14
 */
public class Domain extends Base {
    private static final long serialVersionUID = 5999335867375055580L;
    /**
     * 域名称
     */
    private String            domainName;
    /**
     * 域Code
     */
    private String            domainCode;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }
}
