package cn.icanci.loopstack.rec.admin.web.controller.config;

import cn.icanci.loopstack.rec.admin.biz.service.DomainService;
import cn.icanci.loopstack.rec.admin.web.mapper.DomainWebMapper;
import cn.icanci.loopstack.rec.admin.web.form.DomainQueryForm;
import cn.icanci.loopstack.rec.admin.web.model.Domain;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.DomainVO;
import cn.icanci.loopstack.rec.common.result.R;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 15:10
 */
@RestController
@RequestMapping("/rec/domain")
public class DomainController {
    @Resource
    private DomainService   domainService;
    @Resource
    private DomainWebMapper domainWebMapper;

    // ================================ CRUD ================================
    @PostMapping("query")
    public R query(@RequestBody DomainQueryForm form) {
        return R.builderOk()
            .data("queryPage", domainService.queryPage(domainWebMapper.web2vo(form.getDomain()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize())).build();
    }

    @PostMapping("save")
    public R save(@RequestBody Domain domain) {
        domainService.save(domainWebMapper.web2vo(domain));
        return R.builderOk().build();
    }

    @GetMapping("validateDomainName/{domainName}")
    public R validateDomainName(@PathVariable("domainName") String domainName) {
        DomainVO domain = domainService.queryByDomainName(domainName);
        return R.builderOk().data("result", domain == null).build();
    }

    @GetMapping("validateDomainCode/{domainCode}")
    public R validateDomainCode(@PathVariable("domainCode") String domainCode) {
        DomainVO domain = domainService.queryByDomainCode(domainCode);
        return R.builderOk().data("result", domain == null).build();
    }

    // ================================ Load Selectors ================================

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = domainService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }
}
