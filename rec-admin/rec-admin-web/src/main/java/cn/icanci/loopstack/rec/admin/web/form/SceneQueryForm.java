package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.Scene;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 16:57
 */
public class SceneQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = 4601779530670449448L;

    private Scene             scene;

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
