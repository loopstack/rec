package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.BaseData;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 11:24
 */
public class BaseDataQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = 2911663907403903086L;

    private BaseData          baseData;

    public BaseData getBaseData() {
        return baseData;
    }

    public void setBaseData(BaseData baseData) {
        this.baseData = baseData;
    }
}
