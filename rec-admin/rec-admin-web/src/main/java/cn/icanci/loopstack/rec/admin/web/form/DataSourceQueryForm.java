package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.DataSource;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/14 08:40
 */
public class DataSourceQueryForm extends BaseQueryForm {

    private static final long serialVersionUID = -5707057820546333067L;

    private DataSource        dataSource;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
