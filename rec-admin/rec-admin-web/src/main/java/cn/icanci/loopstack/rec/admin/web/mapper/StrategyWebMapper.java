package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.*;
import cn.icanci.loopstack.rec.admin.web.model.Strategy;
import cn.icanci.loopstack.rec.common.model.config.StrategyVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:49
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, RuleTypeEnumConverter.class, OperatorEnumConverter.class, InterruptEnumConverter.class,
                                            ResultTypeEnumConverter.class, RuleModeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface StrategyWebMapper extends BaseWebMapper<Strategy, StrategyVO> {
    StrategyVO.RuleListInfo web2vo(StrategyVO.RuleListInfo ruleListInfo);

    Strategy.RuleListInfo vo2web(StrategyVO.RuleListInfo ruleListInfo);

    StrategyVO.RuleTreeInfo web2vo(StrategyVO.RuleTreeInfo ruleTreeInfo);

    Strategy.RuleTreeInfo vo2web(StrategyVO.RuleTreeInfo ruleTreeInfo);

    StrategyVO.Condition web2vo(Strategy.Condition condition);

    Strategy.Condition vo2web(StrategyVO.Condition condition);

    StrategyVO.ConditionCell web2vo(Strategy.ConditionCell conditionCell);

    Strategy.ConditionCell vo2web(StrategyVO.ConditionCell conditionCell);

}
