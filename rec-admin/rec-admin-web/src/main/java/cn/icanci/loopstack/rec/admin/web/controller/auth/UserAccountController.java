package cn.icanci.loopstack.rec.admin.web.controller.auth;

import cn.icanci.loopstack.rec.admin.web.model.AdminLogin;
import cn.icanci.loopstack.rec.common.result.R;
import cn.icanci.loopstack.rec.common.result.ResultCodes;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * TODO 权限暂未设计
 * 用户登录服务
 * 
 * @author icanci
 * @since 1.0 Created in 2022/04/25 16:40
 */
@SuppressWarnings("all")
@RestController
@RequestMapping("/rec/user")
public class UserAccountController {
    private Logger logger = LoggerFactory.getLogger(UserAccountController.class);

    @PostMapping("/login")
    public R login(@RequestBody(required = false) AdminLogin loginVO) {
        try {
            AdminLogin vo = new AdminLogin();
            vo.setId(1);
            vo.setUsername("admin");
            vo.setPassword("admin");
            vo.setSecurity(1);
            vo.setCreateTime(new Date());
            vo.setRoles("[admin]");
            vo.setAvatar("http://localhost:9999/static/head.jpg");
            return R.builderOk().message("登录成功").data("userInfo", vo).build();
        } catch (Exception e) {
            return R.builderFail().message(e.getMessage()).build();
        }
    }

    @GetMapping("/info/{username}")
    public R info(@PathVariable("username") String username) {
        if (StringUtils.isEmpty(username)) {
            return R.builderOk().code(ResultCodes.LOGIN_TIME_OUT).message("用户已经退出").build();
        }
        AdminLogin vo = new AdminLogin();
        vo.setId(1);
        vo.setUsername("admin");
        vo.setPassword("admin");
        vo.setSecurity(1);
        vo.setCreateTime(new Date());
        vo.setRoles("[admin]");
        vo.setAvatar("http://localhost:9999/static/head.jpg");
        return R.builderOk().data("roles", vo.getRoles()).data("name", vo.getUsername()).data("avatar", vo.getAvatar()).build();
    }

    @PostMapping("/logout")
    public R logout() {
        return R.builderOk().message("退出成功").build();
    }
}
