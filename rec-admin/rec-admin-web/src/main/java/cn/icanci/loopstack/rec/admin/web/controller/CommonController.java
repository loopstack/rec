package cn.icanci.loopstack.rec.admin.web.controller;

import cn.icanci.loopstack.rec.common.enums.*;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.result.R;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 20:12
 */
@RestController
@RequestMapping("/rec/common")
public class CommonController {

    @GetMapping("dataSourceType")
    public R dataSourceType() {
        List<TextValue> textValues = Arrays.stream(DataSourceTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("dataType")
    public R dataType() {
        List<TextValue> textValues = Arrays.stream(DataTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("httpRequestType")
    public R httpRequestType() {
        List<TextValue> textValues = Arrays.stream(HttpRequestTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("interrupt")
    public R interrupt() {
        List<TextValue> textValues = Arrays.stream(InterruptEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("operator")
    public R operator() {
        List<TextValue> textValues = Arrays.stream(OperatorEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("resultType")
    public R resultType() {
        List<TextValue> textValues = Arrays.stream(ResultTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("ruleType")
    public R ruleType() {
        List<TextValue> textValues = Arrays.stream(RuleTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("scriptType")
    public R scriptType() {
        List<TextValue> textValues = Arrays.stream(ScriptTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("ruleMode")
    public R ruleMode() {
        List<TextValue> textValues = Arrays.stream(RuleModeEnum.values()).map(x -> new TextValue(x.getDesc(), x.name())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

}
