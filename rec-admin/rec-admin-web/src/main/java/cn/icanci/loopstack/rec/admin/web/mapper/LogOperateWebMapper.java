package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.web.model.LogOperate;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;

import java.util.Collection;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:27
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface LogOperateWebMapper {
    default LogOperate vo2web(LogOperateVO vo) {
        LogOperate logOperate = new LogOperate();
        logOperate.setId(vo.getId());
        logOperate.setModule(vo.getModule().getDesc());
        logOperate.setTargetId(vo.getTargetId());
        logOperate.setOperatorType(vo.getOperatorType().getDesc());
        logOperate.setContent(vo.getContent());
        logOperate.setCreateTime(vo.getCreateTime());
        logOperate.setEnv(logOperate.getEnv());
        return logOperate;
    }

    List<LogOperate> vos2webs(Collection<LogOperateVO> vos);
}
