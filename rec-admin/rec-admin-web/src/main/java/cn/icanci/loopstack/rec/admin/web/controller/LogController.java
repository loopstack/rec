package cn.icanci.loopstack.rec.admin.web.controller;

import cn.icanci.loopstack.rec.admin.biz.service.LogOperateService;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.Paginator;
import cn.icanci.loopstack.rec.admin.web.mapper.LogOperateWebMapper;
import cn.icanci.loopstack.rec.admin.web.form.LogQueryForm;
import cn.icanci.loopstack.rec.admin.web.model.LogOperate;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;
import cn.icanci.loopstack.rec.common.result.R;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:15
 */
@RestController
@RequestMapping("/rec/log")
public class LogController {
    @Resource
    private LogOperateService   logOperateService;
    @Resource
    private LogOperateWebMapper logOperateWebMapper;

    @PostMapping("/query")
    public R query(@RequestBody LogQueryForm form) {
        LogOperate logOperate = form.getLogOperate();
        Paginator paginator = form.getPaginator();
        PageList<LogOperateVO> pageList = logOperateService.queryPage(logOperate.getModule(), logOperate.getTargetId(), paginator.getCurrentPage(), paginator.getPageSize());
        PageList<LogOperate> ret = new PageList<>(logOperateWebMapper.vos2webs(pageList.getData()), pageList.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }
}
