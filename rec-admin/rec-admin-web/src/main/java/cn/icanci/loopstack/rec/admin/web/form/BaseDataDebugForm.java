package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.BaseData;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 21:57
 */
public class BaseDataDebugForm implements Serializable {
    private static final long serialVersionUID = -191495744900802008L;

    private BaseData          baseData;
    private String            scriptContentTest;

    public BaseData getBaseData() {
        return baseData;
    }

    public void setBaseData(BaseData baseData) {
        this.baseData = baseData;
    }

    public String getScriptContentTest() {
        return scriptContentTest;
    }

    public void setScriptContentTest(String scriptContentTest) {
        this.scriptContentTest = scriptContentTest;
    }
}
