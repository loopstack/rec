package cn.icanci.loopstack.rec.admin.web.mapper;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataSourceTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.HttpRequestTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.web.model.DataSource;
import cn.icanci.loopstack.rec.common.model.config.DataSourceVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:29
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            HttpRequestTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DataSourceWebMapper extends BaseWebMapper<DataSource, DataSourceVO> {

    DataSourceVO.ScriptInfo web2vo(DataSource.ScriptInfo scriptInfo);

    DataSource.ScriptInfo vo2web(DataSourceVO.ScriptInfo scriptInfo);

    DataSourceVO.HttpInfo web2vo(DataSource.HttpInfo httpInfo);

    DataSource.HttpInfo vo2web(DataSourceVO.HttpInfo httpInfo);

    DataSourceVO.SqlInfo web2vo(DataSource.SqlInfo sqlInfo);

    DataSource.SqlInfo vo2web(DataSourceVO.SqlInfo sqlInfo);
}
