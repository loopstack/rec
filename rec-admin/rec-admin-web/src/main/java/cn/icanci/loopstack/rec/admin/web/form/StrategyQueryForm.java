package cn.icanci.loopstack.rec.admin.web.form;

import cn.icanci.loopstack.rec.admin.web.model.Strategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:08
 */
public class StrategyQueryForm extends BaseQueryForm {
    private static final long     serialVersionUID = 6978323388657447739L;
    private              Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
