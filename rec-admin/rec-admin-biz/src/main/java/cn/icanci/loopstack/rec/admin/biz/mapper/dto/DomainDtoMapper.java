package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DomainDO;
import cn.icanci.loopstack.rec.common.aggregation.model.DomainDTO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DomainDtoMapper extends BaseDtoMapper<DomainDO, DomainDTO> {
}
