package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.*;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.StrategyDO;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, RuleTypeEnumConverter.class, OperatorEnumConverter.class, InterruptEnumConverter.class,
                                            ResultTypeEnumConverter.class, RuleModeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface StrategyDtoMapper extends BaseDtoMapper<StrategyDO, StrategyDTO> {

    StrategyDTO.RuleListInfo do2dto(StrategyDO.RuleListInfo ruleListInfo);

    StrategyDTO.RuleTreeInfo do2dto(StrategyDO.RuleTreeInfo ruleTreeInfo);

    StrategyDTO.Condition do2dto(StrategyDO.Condition condition);

    StrategyDTO.ConditionCell do2dto(StrategyDO.ConditionCell conditionCell);

}
