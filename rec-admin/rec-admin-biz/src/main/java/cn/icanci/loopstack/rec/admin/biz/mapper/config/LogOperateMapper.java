package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ModuleTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.LogOperateDO;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:19
 */
@Mapper(componentModel = "spring", uses = { ModuleTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface LogOperateMapper extends BaseMapper<LogOperateDO, LogOperateVO> {
}
