package cn.icanci.loopstack.rec.admin.biz.mapper.convertor;

import cn.icanci.loopstack.rec.common.enums.DataTypeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:00
 */
@Component
public class DataTypeEnumConverter extends AbstractBaseConverter<DataTypeEnum> {

}
