package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import java.util.Collection;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:20
 */
public interface BaseMapper<T, R> {

    R do2vo(T t);

    List<R> dos2vos(Collection<T> ts);

    T vo2do(R r);

    List<T> vos2dos(List<R> rs);
}
