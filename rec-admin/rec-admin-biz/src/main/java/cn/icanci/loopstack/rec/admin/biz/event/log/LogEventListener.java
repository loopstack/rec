package cn.icanci.loopstack.rec.admin.biz.event.log;

import cn.icanci.loopstack.rec.admin.biz.service.LogOperateService;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;
import cn.icanci.loopstack.rec.spi.event.BaseEventListener;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

/**
 * 日志事件监听
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:50
 */
@Component
public class LogEventListener extends BaseEventListener<LogEvent> {

    @Resource
    private LogOperateService logOperateService;

    @Override
    protected void event(LogEvent event) {
        LogOperateVO logOperateVO = new LogOperateVO();
        logOperateVO.setModule(event.getModuleType());
        logOperateVO.setTargetId(event.getTargetId());
        logOperateVO.setOperatorType(event.getLogOperatorType());
        logOperateVO.setContent(event.getContext());
        logOperateVO.setCreateTime(event.getCreateTime());

        logOperateService.log(logOperateVO);
    }
}
