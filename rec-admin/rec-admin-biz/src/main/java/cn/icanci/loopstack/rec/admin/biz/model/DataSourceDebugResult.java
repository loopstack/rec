package cn.icanci.loopstack.rec.admin.biz.model;

import java.io.Serializable;

/**
 * 数据源Debug返回结果
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 14:47
 */
public class DataSourceDebugResult implements Serializable {
    private static final long serialVersionUID = -3567901817148802718L;
    /**
     * 是否执行成功
     */
    private boolean           success;
    /**
     * 数据源类型
     */
    private String            dataSourceType;
    /**
     * 脚本类型
     *
     * 数据源类型为脚本的时候，scriptType 不为空
     */
    private String            scriptType;
    /**
     * 实际执行结果
     */
    private Object            realResult;
    /**
     * 执行过程中出现的异常
     */
    private String            exceptionMessage;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDataSourceType() {
        return dataSourceType;
    }

    public void setDataSourceType(String dataSourceType) {
        this.dataSourceType = dataSourceType;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public Object getRealResult() {
        return realResult;
    }

    public void setRealResult(Object realResult) {
        this.realResult = realResult;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
