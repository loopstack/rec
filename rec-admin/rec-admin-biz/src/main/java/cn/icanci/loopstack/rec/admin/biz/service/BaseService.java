package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.config.BaseVO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * MongoDB Base Service for CRUD
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:51
 */
public interface BaseService<T extends BaseVO> {
    /** label 格式化 */
    String DELETED_FORMAT     = "[无效] %s";

    /** label 格式化 */
    String NOT_DELETED_FORMAT = "[有效] %s";

    /**
     * 查询所有 T
     *
     * @return 返回所有T
     */
    List<T> queryAll();

    /**
     * 保存或者更新一条信息
     *
     * @param t t
     */
    void save(T t);

    /**
     * 根据id进行查询
     *
     * @param id id
     * @return 返回查询的结果
     */
    T queryById(String id);

    /**
     * 分页查询
     *
     * @param t T
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 返回分页查询结果
     */
    PageList<T> queryPage(T t, int pageNum, int pageSize);

    /**
     * 是否是插入
     * 
     * @param t t
     * @return 返回是否是插入
     */
    default boolean doInsert(T t) {
        return StringUtils.isBlank(t.getId());
    }

    /**
     * 是否是删除的
     * 
     * @param t t
     * @return 返回是否是删除的
     */
    default boolean isDeleted(T t) {
        return t.getIsDelete() == 1;
    }
}
