package cn.icanci.loopstack.rec.admin.biz.event;

import cn.icanci.loopstack.rec.spi.event.BaseEvent;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:50
 */
public class RecEvent extends BaseEvent {

    private static final long serialVersionUID = -1279926691854740196L;

    public RecEvent() {
        super("RECEvent");
    }
}
