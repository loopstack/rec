package cn.icanci.loopstack.rec.admin.biz.thread;

import cn.icanci.loopstack.rec.admin.biz.service.LockService;
import cn.icanci.loopstack.rec.admin.biz.service.RegisterService;

import javax.annotation.Resource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 17:34
 */
@Service
public class TriggerThreadStart implements InitializingBean {
    @Resource
    private RegisterService registerService;
    @Resource
    private LockService     lockService;

    @Override
    public void afterPropertiesSet() throws Exception {

        TriggerThread.setRegisterService(registerService);

        TriggerThread.setLockService(lockService);

        TriggerThread.start();
    }
}
