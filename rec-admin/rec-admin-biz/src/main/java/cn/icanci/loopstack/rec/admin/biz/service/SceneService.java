package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.SceneVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:21
 */
public interface SceneService extends BaseService<SceneVO> {
    @Override
    List<SceneVO> queryAll();

    @Override
    void save(SceneVO sceneVO);

    @Override
    SceneVO queryById(String id);

    @Override
    PageList<SceneVO> queryPage(SceneVO sceneVO, int pageNum, int pageSize);

    /**
     * 根据域Code查询场景信息
     * 
     * @param domainCode domainCode
     * @return 返回场景信息
     */
    SceneVO queryByDomainCode(String domainCode);

    /**
     * 加载场景
     * 
     * @return 加载场景
     */
    List<TextValue> loadSelector(String domainCode);
}
