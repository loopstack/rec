package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.biz.model.BaseDataDebugResult;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.BaseDataVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:57
 */
public interface BaseDataService extends BaseService<BaseDataVO> {
    @Override
    List<BaseDataVO> queryAll();

    @Override
    void save(BaseDataVO baseDataVO);

    @Override
    BaseDataVO queryById(String id);

    @Override
    PageList<BaseDataVO> queryPage(BaseDataVO baseDataVO, int pageNum, int pageSize);

    /**
     * BaseDataVO
     *
     * @param filedName filedName
     * @param domainCode domainCode
     * @return BaseDataVO
     */
    BaseDataVO queryByFieldNameAndDomainCode(String filedName, String domainCode);

    /**
     * 加载下拉框
     *
     * @param domainCode domainCode
     * @return 返回下拉框数据
     */
    List<TextValue> loadSelector(String domainCode);

    /**
     * 测试
     * 
     * @param baseData baseData
     * @param scriptContentTest 测试参数
     * @return BaseDataDebugResult
     */
    BaseDataDebugResult debug(BaseDataVO baseData, String scriptContentTest);

    BaseDataVO baseDataQueryByUuid(String uuid);
}
