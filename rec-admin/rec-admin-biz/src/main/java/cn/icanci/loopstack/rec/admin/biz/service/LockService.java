package cn.icanci.loopstack.rec.admin.biz.service;

/**
 * MongoDB实现分布式锁
 *
 * Tips: 分布式锁的实现是必要的，因为客户端不知道有多少；服务端也不知道有多少
 *       所以在进行心跳检测的时候，需要这样进行处理，否则会消耗无所谓的带宽
 *       
 * @author icanci
 * @since 1.0 Created in 2022/11/25 21:10
 */
public interface LockService {

    String acquire(String key, long expiration);

    boolean release(String key, String token);

    boolean refresh(String key, String token, long expiration);

}
