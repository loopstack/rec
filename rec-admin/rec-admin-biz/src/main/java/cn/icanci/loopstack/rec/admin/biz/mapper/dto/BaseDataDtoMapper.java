package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ResultTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.BaseDataDO;
import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
@Mapper(componentModel = "spring", uses = { DataTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            ResultTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface BaseDataDtoMapper extends BaseDtoMapper<BaseDataDO, BaseDataDTO> {
}
