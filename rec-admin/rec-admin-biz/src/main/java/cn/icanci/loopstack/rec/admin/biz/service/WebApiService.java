package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.common.aggregation.model.*;

import java.util.List;
import java.util.Set;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:49
 */
public interface WebApiService {

    List<DomainDTO> loadDomainByDomains(Set<String> domainCodes);

    List<SceneDTO> loadSceneByDomains(Set<String> domainCodes);

    List<MetadataDTO> loadMetadataByDomains(Set<String> domainCodes);

    List<BaseDataDTO> loadBaseDataByDomains(Set<String> domainCodes);

    List<DataSourceDTO> loadDataSourceByDomains(Set<String> domainCodes);

    List<StrategyDTO> loadStrategyByDomains(Set<String> domainCodes);

    Set<String> loadAllDomainCodes();
}
