package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.RegisterDO;
import cn.icanci.loopstack.rec.common.model.config.RegisterVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 22:06
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface RegisterMapper extends BaseMapper<RegisterDO, RegisterVO> {
}
