package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataSourceTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.HttpRequestTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DataSourceDO;
import cn.icanci.loopstack.rec.common.model.config.DataSourceVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:29
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            HttpRequestTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DataSourceMapper extends BaseMapper<DataSourceDO, DataSourceVO> {

    DataSourceVO.ScriptInfo do2vo(DataSourceDO.ScriptInfo scriptInfo);

    DataSourceDO.ScriptInfo vo2do(DataSourceVO.ScriptInfo scriptInfo);

    DataSourceVO.HttpInfo do2vo(DataSourceDO.HttpInfo httpInfo);

    DataSourceDO.HttpInfo vo2do(DataSourceVO.HttpInfo httpInfo);

    DataSourceVO.SqlInfo do2vo(DataSourceDO.SqlInfo sqlInfo);

    DataSourceDO.SqlInfo vo2do(DataSourceVO.SqlInfo sqlInfo);
}
