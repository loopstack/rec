package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:41
 */
public interface LogOperateService {
    /**
     * 记录日志
     * 
     * @param logOperate logOperate
     */
    void log(LogOperateVO logOperate);

    /**
     * queryPage
     * 
     * @param module module
     * @param targetId targetId
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return PageList<LogOperateVO>
     */
    PageList<LogOperateVO> queryPage(String module, String targetId, int pageNum, int pageSize);
}
