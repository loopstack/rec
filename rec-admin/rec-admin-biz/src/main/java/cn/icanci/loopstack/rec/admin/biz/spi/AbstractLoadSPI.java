package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.admin.biz.service.WebApiService;
import cn.icanci.loopstack.rec.engine.sdk.extensions.SpringBean;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:39
 */
@SpringBean({ WebApiService.class })
public abstract class AbstractLoadSPI {
    protected WebApiService webApiService;
}
