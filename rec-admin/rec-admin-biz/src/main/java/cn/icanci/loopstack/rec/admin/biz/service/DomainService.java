package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.DomainVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 15:13
 */
public interface DomainService extends BaseService<DomainVO> {

    /**
     * 查询所有Domain
     * 
     * @return 返回所有Domain
     */
    List<DomainVO> queryAll();

    /**
     * 保存或者更新一条信息
     * 
     * @param domain domain
     */
    void save(DomainVO domain);

    /**
     * 根据id进行查询
     * 
     * @param id id
     * @return 返回查询的结果
     */
    DomainVO queryById(String id);

    /**
     * 分页查询
     * 
     * @param domain domain
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 返回分页查询结果
     */
    PageList<DomainVO> queryPage(DomainVO domain, int pageNum, int pageSize);

    /**
     * 根据DomainName查询
     *
     * @param domainName domainName
     * @return 返回查询的结果
     */
    DomainVO queryByDomainName(String domainName);

    /**
     * 根据domainCode查询
     *
     * @param domainCode domainCode
     * @return 返回查询的结果
     */
    DomainVO queryByDomainCode(String domainCode);

    /**
     * 加载页面下拉框
     * 
     * @return 返回页面下拉框
     */
    List<TextValue> loadSelector();
}
