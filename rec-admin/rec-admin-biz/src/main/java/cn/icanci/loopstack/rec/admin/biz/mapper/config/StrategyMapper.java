package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.*;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.StrategyDO;
import cn.icanci.loopstack.rec.common.model.config.StrategyVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:49
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, RuleTypeEnumConverter.class, OperatorEnumConverter.class, InterruptEnumConverter.class,
                                            ResultTypeEnumConverter.class, RuleModeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface StrategyMapper extends BaseMapper<StrategyDO, StrategyVO> {
    StrategyVO.RuleListInfo do2vo(StrategyDO.RuleListInfo ruleListInfo);

    StrategyDO.RuleListInfo vo2do(StrategyVO.RuleListInfo ruleListInfo);

    StrategyVO.RuleTreeInfo do2vo(StrategyDO.RuleTreeInfo ruleTreeInfo);

    StrategyDO.RuleTreeInfo vo2do(StrategyVO.RuleTreeInfo ruleTreeInfo);

    StrategyVO.Condition do2vo(StrategyDO.Condition condition);

    StrategyDO.Condition vo2do(StrategyVO.Condition condition);

    StrategyVO.ConditionCell do2vo(StrategyDO.ConditionCell conditionCell);

    StrategyDO.ConditionCell vo2do(StrategyVO.ConditionCell conditionCell);

}
