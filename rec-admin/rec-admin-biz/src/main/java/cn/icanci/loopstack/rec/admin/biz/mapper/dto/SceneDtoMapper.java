package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.SceneDO;
import cn.icanci.loopstack.rec.common.aggregation.model.SceneDTO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface SceneDtoMapper extends BaseDtoMapper<SceneDO, SceneDTO> {

    SceneDTO.ScenePair do2dto(SceneDO.ScenePair scenePair);

}
