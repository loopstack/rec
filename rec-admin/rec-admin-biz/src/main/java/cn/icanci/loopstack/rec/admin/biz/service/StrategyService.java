package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.biz.model.StrategyDebugResult;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.config.StrategyVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:37
 */
public interface StrategyService extends BaseService<StrategyVO> {
    @Override
    List<StrategyVO> queryAll();

    @Override
    void save(StrategyVO strategyVO);

    @Override
    StrategyVO queryById(String id);

    @Override
    PageList<StrategyVO> queryPage(StrategyVO strategyVO, int pageNum, int pageSize);

    /**
     * 根据domainCode和strategyName查询
     *
     * @param domainCode domainCode
     * @param strategyName strategyName
     * @return 返回查询结果
     */
    StrategyVO queryByStrategyName(String domainCode, String strategyName);

    /**
     * 对策略进行测试
     *
     * @param strategy strategy
     * @param scriptContentTest 测试参数
     * @return 返回测试结果
     */
    StrategyDebugResult debug(StrategyVO strategy, String scriptContentTest);

    /**
     * 根据domainCode和sceneCode查询
     *
     * @param domainCode domainCode
     * @param sceneCode sceneCode
     * @return 返回查询结果
     */
    StrategyVO queryBySceneCode(String domainCode, String sceneCode);
}
