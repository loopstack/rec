package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.DomainDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.DomainSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminDomainSPI extends AbstractLoadSPI implements DomainSPI {

    @Override
    public DomainDTO loadOne(String domain) {
        return webApiService.loadDomainByDomains(Sets.newHashSet(domain)).iterator().next();
    }

    @Override
    public Set<String> loadAllDomainCodes() {
        return webApiService.loadAllDomainCodes();
    }

    @Override
    public List<DomainDTO> load(Set<String> domains) {
        return webApiService.loadDomainByDomains(domains);
    }

    @Override
    public List<DomainDTO> load(String domain) {
        return webApiService.loadDomainByDomains(Sets.newHashSet(domain));
    }
}
