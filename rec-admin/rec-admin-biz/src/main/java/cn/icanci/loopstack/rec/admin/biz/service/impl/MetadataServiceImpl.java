package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.rec.admin.biz.mapper.config.MetadataMapper;
import cn.icanci.loopstack.rec.admin.biz.service.MetadataService;
import cn.icanci.loopstack.rec.admin.biz.event.log.LogEvent;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.MetadataDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.MetadataDO;
import cn.icanci.loopstack.rec.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.MetadataVO;
import cn.icanci.loopstack.rec.spi.event.EventDispatcher;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:18
 */
@Service
public class MetadataServiceImpl implements MetadataService {
    @Resource
    private MetadataDAO     metadataDAO;
    @Resource
    private MetadataMapper  metadataMapper;
    @Resource
    private EventDispatcher eventDispatcher;

    @Override
    public List<MetadataVO> queryAll() {
        return metadataMapper.dos2vos(metadataDAO.queryAll());
    }

    @Override
    public void save(MetadataVO metadata) {
        if (doInsert(metadata)) {
            MetadataDO insert = metadataMapper.vo2do(metadata);
            metadataDAO.insert(insert);
            eventDispatcher.fire(new LogEvent(insert.getUuid(), ModuleTypeEnum.REC_METADATA, JSONUtil.toJsonStr(insert), LogOperatorTypeEnum.CREATE));
        } else {
            metadataDAO.update(metadataMapper.vo2do(metadata));
            eventDispatcher.fire(new LogEvent(metadata.getUuid(), ModuleTypeEnum.REC_METADATA, JSONUtil.toJsonStr(metadata), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public MetadataVO queryById(String id) {
        return metadataMapper.do2vo(metadataDAO.queryOneById(id));
    }

    @Override
    public PageList<MetadataVO> queryPage(MetadataVO metadata, int pageNum, int pageSize) {
        PageList<MetadataDO> pageQuery = metadataDAO.pageQuery(metadataMapper.vo2do(metadata), pageNum, pageSize);
        return new PageList<>(metadataMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public List<TextValue> loadSelector(String domainCode) {
        List<MetadataVO> metadatas = metadataMapper.dos2vos(metadataDAO.queryByDomainCode(domainCode));
        if (CollectionUtils.isEmpty(metadatas)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (MetadataVO metadata : metadatas) {
            String label;
            String metadataName = metadata.getMetadataName();
            if (isDeleted(metadata)) {
                label = String.format(DELETED_FORMAT, metadataName);
            } else {
                label = String.format(NOT_DELETED_FORMAT, metadataName);
            }
            textValues.add(new TextValue(label, metadata.getUuid(), metadata.getMetadataPairs()));
        }
        return textValues;
    }

    @Override
    public MetadataVO queryByMetadataName(String metadataName, String domainCode) {
        MetadataDO metadata = metadataDAO.queryByMetadataName(metadataName, domainCode);
        return metadataMapper.do2vo(metadata);
    }
}