package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.config.RegisterVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 21:23
 */
public interface RegisterService extends BaseService<RegisterVO> {
    @Override
    List<RegisterVO> queryAll();

    @Override
    void save(RegisterVO register);

    @Override
    RegisterVO queryById(String id);

    @Override
    PageList<RegisterVO> queryPage(RegisterVO registerVO, int pageNum, int pageSize);

    void deleteById(String id);

    void doRegister(List<RegisterVO> registers);

    void publish(String uuid, String domainCode);
}
