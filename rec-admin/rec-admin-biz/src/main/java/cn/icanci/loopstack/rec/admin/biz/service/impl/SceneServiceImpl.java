package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.icanci.loopstack.rec.admin.biz.mapper.config.SceneMapper;
import cn.icanci.loopstack.rec.admin.biz.service.SceneService;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.SceneDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.SceneDO;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.SceneVO;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:21
 */
@Service
public class SceneServiceImpl implements SceneService {
    @Resource
    private SceneDAO    sceneDAO;
    @Resource
    private SceneMapper sceneMapper;

    @Override
    public List<SceneVO> queryAll() {
        return sceneMapper.dos2vos(sceneDAO.queryAll());
    }

    @Override
    public void save(SceneVO scene) {
        if (doInsert(scene)) {
            sceneDAO.insert(sceneMapper.vo2do(scene));
        } else {
            sceneDAO.update(sceneMapper.vo2do(scene));
        }
    }

    @Override
    public SceneVO queryById(String id) {
        return sceneMapper.do2vo(sceneDAO.queryOneById(id));
    }

    @Override
    public PageList<SceneVO> queryPage(SceneVO scene, int pageNum, int pageSize) {
        PageList<SceneDO> pageQuery = sceneDAO.pageQuery(sceneMapper.vo2do(scene), pageNum, pageSize);
        return new PageList<>(sceneMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public SceneVO queryByDomainCode(String domainCode) {
        SceneDO scene = sceneDAO.queryByDomainCode(domainCode);
        if (scene == null) {
            SceneVO sceneVO = new SceneVO();
            sceneVO.setDomainCode(domainCode);
            sceneVO.setScenePairs(Lists.newArrayList());
            return sceneVO;
        }
        return sceneMapper.do2vo(scene);
    }

    @Override
    public List<TextValue> loadSelector(String domainCode) {
        SceneVO sceneVO = queryByDomainCode(domainCode);
        if (sceneVO == null || CollectionUtils.isEmpty(sceneVO.getScenePairs())) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (SceneVO.ScenePair scenePair : sceneVO.getScenePairs()) {
            textValues.add(new TextValue(scenePair.getSceneName(), scenePair.getSceneCode()));
        }
        return textValues;
    }
}
