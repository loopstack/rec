package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.SceneDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.SceneSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminSceneSPI extends AbstractLoadSPI implements SceneSPI {
    @Override
    public List<SceneDTO> load(Set<String> domains) {
        return webApiService.loadSceneByDomains(domains);
    }

    @Override
    public List<SceneDTO> load(String domain) {
        return webApiService.loadSceneByDomains(Sets.newHashSet(domain));
    }

    @Override
    public SceneDTO loadOne(String domainCode) {
        return webApiService.loadSceneByDomains(Sets.newHashSet(domainCode)).iterator().next();
    }
}
