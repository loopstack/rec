package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.BaseDataDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.BaseDataSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminBaseDataSPI extends AbstractLoadSPI implements BaseDataSPI {

    @Override
    public List<BaseDataDTO> load(Set<String> domains) {
        return webApiService.loadBaseDataByDomains(domains);
    }

    @Override
    public List<BaseDataDTO> load(String domain) {
        return webApiService.loadBaseDataByDomains(Sets.newHashSet(domain));
    }
}
