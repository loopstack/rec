package cn.icanci.loopstack.rec.admin.biz.model;

import java.io.Serializable;

/**
 * 基础数据Debug返回结果
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/13 14:47
 */
public class BaseDataDebugResult implements Serializable {
    private static final long serialVersionUID = 5879082736696321315L;
    /**
     * 是否执行成功
     */
    private boolean success;
    /**
     * 前端传入的执行结果类型
     */
    private String  reqResultType;
    /**
     * 脚本类型
     */
    private String  scriptType;
    /**
     * 实际执行结果
     */
    private String  realResult;
    /**
     * 执行过程中出现的异常
     */
    private String  exceptionMessage;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getReqResultType() {
        return reqResultType;
    }

    public void setReqResultType(String reqResultType) {
        this.reqResultType = reqResultType;
    }

    public String getRealResult() {
        return realResult;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public void setRealResult(String realResult) {
        this.realResult = realResult;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
