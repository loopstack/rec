package cn.icanci.loopstack.rec.admin.biz.config;

import cn.icanci.loopstack.rec.spi.event.DefaultEventDispatcher;
import cn.icanci.loopstack.rec.spi.event.EventDispatcher;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/13 14:06
 */
@Configuration
public class RecAdminConfig implements ApplicationContextAware, InitializingBean {
    /**
     * Spring 上下文
     */
    private ApplicationContext context;

    /**
     * 事件分发器
     *
     * @return 返回事件分发器
     */
    @Bean("eventDispatcher")
    public EventDispatcher eventDispatcher() {
        return new DefaultEventDispatcher();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // no op
    }
}
