package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import java.util.Collection;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
public interface BaseDtoMapper<T, R> {

    R do2dto(T t);

    List<R> dos2dtos(Collection<T> ts);
}
