package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ResultTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.BaseDataDO;
import cn.icanci.loopstack.rec.common.model.config.BaseDataVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:19
 */
@Mapper(componentModel = "spring", uses = { DataTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            ResultTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface BaseDataMapper extends BaseMapper<BaseDataDO, BaseDataVO> {
}
