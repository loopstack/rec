package cn.icanci.loopstack.rec.admin.biz.mapper.convertor;

import cn.icanci.loopstack.rec.common.enums.RuleModeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/16 21:54
 */
@Component
public class RuleModeEnumConverter extends AbstractBaseConverter<RuleModeEnum> {
}
