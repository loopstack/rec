package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.icanci.loopstack.rec.admin.biz.mapper.dto.*;
import cn.icanci.loopstack.rec.admin.biz.service.WebApiService;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.*;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DomainDO;
import cn.icanci.loopstack.rec.common.aggregation.model.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:49
 */
@Service
public class WebApiServiceImpl implements WebApiService {
    // =========================== DAO  ===========================
    @Resource
    private DomainDAO           domainDAO;
    @Resource
    private SceneDAO            sceneDAO;
    @Resource
    private MetadataDAO         metadataDAO;
    @Resource
    private BaseDataDAO         baseDataDAO;
    @Resource
    private DataSourceDAO       dataSourceDAO;
    @Resource
    private StrategyDAO         strategyDAO;
    // =========================== DTO mapper ===========================
    @Resource
    private DomainDtoMapper     domainDtoMapper;
    @Resource
    private SceneDtoMapper      sceneDtoMapper;
    @Resource
    private MetadataDtoMapper   metadataDtoMapper;
    @Resource
    private BaseDataDtoMapper   baseDataDtoMapper;
    @Resource
    private DataSourceDtoMapper dataSourceDtoMapper;
    @Resource
    private StrategyDtoMapper   strategyDtoMapper;

    // =========================== Load Method ===========================

    @Override
    public List<DomainDTO> loadDomainByDomains(Set<String> domainCodes) {
        List<DomainDTO> domains = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            domains.add(domainDtoMapper.do2dto(domainDAO.queryByDomainCode(domainCode)));
        }
        return domains.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public List<SceneDTO> loadSceneByDomains(Set<String> domainCodes) {
        List<SceneDTO> scenes = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            scenes.add(sceneDtoMapper.do2dto(sceneDAO.queryByDomainCode(domainCode)));
        }
        return scenes.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public List<MetadataDTO> loadMetadataByDomains(Set<String> domainCodes) {
        List<MetadataDTO> metadatas = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            metadatas.addAll(metadataDtoMapper.dos2dtos(metadataDAO.queryByDomainCode(domainCode)));
        }
        return metadatas.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public List<BaseDataDTO> loadBaseDataByDomains(Set<String> domainCodes) {
        List<BaseDataDTO> baseDatas = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            baseDatas.addAll(baseDataDtoMapper.dos2dtos(baseDataDAO.queryByDomainCode(domainCode)));
        }
        return baseDatas.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public List<DataSourceDTO> loadDataSourceByDomains(Set<String> domainCodes) {
        List<DataSourceDTO> dataSources = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            dataSources.addAll(dataSourceDtoMapper.dos2dtos(dataSourceDAO.queryByDomainCode(domainCode)));
        }
        return dataSources.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public List<StrategyDTO> loadStrategyByDomains(Set<String> domainCodes) {
        List<StrategyDTO> strategies = Lists.newArrayList();
        for (String domainCode : domainCodes) {
            strategies.addAll(strategyDtoMapper.dos2dtos(strategyDAO.queryByDomainCode(domainCode)));
        }
        return strategies.stream().filter(x -> x.getIsDelete() == 0).collect(Collectors.toList());
    }

    @Override
    public Set<String> loadAllDomainCodes() {
        return domainDAO.queryAll().stream().filter(x -> x.getIsDelete() == 0).map(DomainDO::getDomainCode).collect(Collectors.toSet());
    }
}
