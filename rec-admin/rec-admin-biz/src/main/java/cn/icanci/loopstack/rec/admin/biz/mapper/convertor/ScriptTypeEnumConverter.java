package cn.icanci.loopstack.rec.admin.biz.mapper.convertor;

import cn.icanci.loopstack.rec.common.enums.ScriptTypeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:16
 */
@Component
public class ScriptTypeEnumConverter extends AbstractBaseConverter<ScriptTypeEnum> {
}
