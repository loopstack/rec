package cn.icanci.loopstack.rec.admin.biz.model;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 22:35
 */
public class StrategyDebugResult implements Serializable {
    private static final long serialVersionUID = -5412720512170304475L;
    /** 是否执行成功   */
    public boolean            success;

    /** 返回值是否是布尔类型 */
    public boolean            retBoolean;

    /** 返回值Fastjson序列化值 */
    public Object             retValue;

    /** 执行异常结果 */
    public String             exceptionMessage;

    /** TODO 实际执行请求参数 */
    public String             executorParam;

    public boolean isRetBoolean() {
        if (retValue == null) {
            return false;
        }
        if (retValue instanceof Boolean) {
            return true;
        }
        if (retValue instanceof String) {
            return StringUtils.equalsIgnoreCase(retValue.toString(), Boolean.TRUE.toString()) //
                   || StringUtils.equalsIgnoreCase(retValue.toString(), Boolean.FALSE.toString());
        }
        return false;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getRetValue() {
        return retValue;
    }

    public void setRetValue(Object retValue) {
        this.retValue = retValue;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExecutorParam() {
        return executorParam;
    }

    public void setExecutorParam(String executorParam) {
        this.executorParam = executorParam;
    }
}
