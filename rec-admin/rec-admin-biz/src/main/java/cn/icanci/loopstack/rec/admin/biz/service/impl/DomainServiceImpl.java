package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.rec.admin.biz.mapper.config.DomainMapper;
import cn.icanci.loopstack.rec.admin.biz.service.DomainService;
import cn.icanci.loopstack.rec.admin.biz.event.log.LogEvent;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.DomainDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DomainDO;
import cn.icanci.loopstack.rec.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.DomainVO;
import cn.icanci.loopstack.rec.spi.event.EventDispatcher;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * Domain Service
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 15:13
 */
@Service
public class DomainServiceImpl implements DomainService {
    @Resource
    private DomainDAO       domainDAO;
    @Resource
    private DomainMapper    domainMapper;
    @Resource
    private EventDispatcher eventDispatcher;

    @Override
    public List<DomainVO> queryAll() {
        return domainMapper.dos2vos(domainDAO.queryAll());
    }

    @Override
    public void save(DomainVO domain) {
        if (doInsert(domain)) {
            DomainDO insert = domainMapper.vo2do(domain);
            domainDAO.insert(insert);
            eventDispatcher.fire(new LogEvent(insert.getUuid(), ModuleTypeEnum.REC_DOMAIN, JSONUtil.toJsonStr(insert), LogOperatorTypeEnum.CREATE));
        } else {
            domainDAO.update(domainMapper.vo2do(domain));
            eventDispatcher.fire(new LogEvent(domain.getUuid(), ModuleTypeEnum.REC_DOMAIN, JSONUtil.toJsonStr(domain), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public DomainVO queryById(String id) {
        return domainMapper.do2vo(domainDAO.queryOneById(id));
    }

    @Override
    public PageList<DomainVO> queryPage(DomainVO domain, int pageNum, int pageSize) {
        PageList<DomainDO> pageQuery = domainDAO.pageQuery(domainMapper.vo2do(domain), pageNum, pageSize);
        return new PageList<>(domainMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public DomainVO queryByDomainName(String domainName) {
        return domainMapper.do2vo(domainDAO.queryByDomainName(domainName));
    }

    @Override
    public DomainVO queryByDomainCode(String domainCode) {
        return domainMapper.do2vo(domainDAO.queryByDomainCode(domainCode));
    }

    @Override
    public List<TextValue> loadSelector() {
        List<DomainVO> domains = queryAll();
        if (CollectionUtils.isEmpty(domains)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (DomainVO domain : domains) {
            String label;
            if (isDeleted(domain)) {
                label = String.format(DELETED_FORMAT, domain.getDomainName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, domain.getDomainName());
            }
            String value = domain.getDomainCode();
            textValues.add(new TextValue(label, value));
        }
        return textValues;
    }
}
