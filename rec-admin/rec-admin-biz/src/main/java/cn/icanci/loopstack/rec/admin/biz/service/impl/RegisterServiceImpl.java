package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.icanci.loopstack.rec.admin.biz.mapper.config.RegisterMapper;
import cn.icanci.loopstack.rec.admin.biz.service.RegisterService;
import cn.icanci.loopstack.rec.admin.biz.thread.TriggerThread;
import cn.icanci.loopstack.rec.admin.biz.event.log.LogEvent;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.RegisterDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.RegisterDO;
import cn.icanci.loopstack.rec.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.rec.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.rec.common.model.config.RegisterVO;
import cn.icanci.loopstack.rec.spi.event.EventDispatcher;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 21:38
 */
@Service
public class RegisterServiceImpl implements RegisterService, InitializingBean {
    @Resource
    private RegisterDAO     registerDAO;
    @Resource
    private RegisterMapper  registerMapper;
    @Resource
    private EventDispatcher eventDispatcher;

    @Override
    public List<RegisterVO> queryAll() {
        return registerMapper.dos2vos(registerDAO.queryAll());
    }

    @Override
    public void save(RegisterVO register) {
        if (doInsert(register)) {
            RegisterDO insert = registerMapper.vo2do(register);
            registerDAO.insert(insert);
        } else {
            registerDAO.update(registerMapper.vo2do(register));
        }
    }

    @Override
    public RegisterVO queryById(String id) {
        return registerMapper.do2vo(registerDAO.queryOneById(id));
    }

    @Override
    public PageList<RegisterVO> queryPage(RegisterVO register, int pageNum, int pageSize) {
        PageList<RegisterDO> pageQuery = registerDAO.pageQuery(registerMapper.vo2do(register), pageNum, pageSize);
        return new PageList<>(registerMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public void deleteById(String id) {
        registerDAO.deleteById(id);
    }

    @Override
    public void doRegister(List<RegisterVO> registers) {
        // 查询，如果有，则更新时间，没有则插入
        // TODO 查询是否有此域，如果没有，则不能注册
        //      返回注册失败的域给SDK
        for (RegisterVO register : registers) {
            RegisterDO dbRegister = registerDAO.queryUnionOne(register.getDomain(), register.getClientAddress(), register.getClientPort());
            if (dbRegister == null) {
                save(register);
            } else {
                dbRegister.setLastUpdateTime(new Date());
                dbRegister.setIsDelete(0);
                dbRegister.setAppName(register.getAppName());
                registerDAO.update(dbRegister);
            }
        }
    }

    @Override
    public void publish(String uuid, String domainCode) {
        List<RegisterDO> registers = registerDAO.queryByDomainCode(domainCode);
        TriggerThread.trigger(registerMapper.dos2vos(registers));
        eventDispatcher.fire(new LogEvent(uuid, ModuleTypeEnum.REC_DOMAIN, "域：" + domainCode + " 发布", LogOperatorTypeEnum.PUBLISH));
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        TriggerThread.setRegisterService(this);
    }
}
