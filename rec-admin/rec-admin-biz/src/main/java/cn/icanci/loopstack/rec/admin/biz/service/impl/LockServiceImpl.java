package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.icanci.loopstack.rec.admin.biz.service.LockService;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.LockDAO;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/26 16:37
 */
@Service
public class LockServiceImpl implements LockService {

    @Resource
    private LockDAO lockDAO;

    @Override
    public String acquire(String key, long expiration) {
        return lockDAO.lock(key, expiration);
    }

    @Override
    public boolean release(String key, String token) {
        return lockDAO.release(key, token);
    }

    @Override
    public boolean refresh(String key, String token, long expiration) {
        return lockDAO.refresh(key, token, expiration);
    }
}
