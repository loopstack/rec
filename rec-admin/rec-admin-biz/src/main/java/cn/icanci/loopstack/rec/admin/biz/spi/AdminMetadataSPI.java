package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.MetadataDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.MetadataSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminMetadataSPI extends AbstractLoadSPI implements MetadataSPI {
    @Override
    public List<MetadataDTO> load(Set<String> domains) {
        return webApiService.loadMetadataByDomains(domains);
    }

    @Override
    public List<MetadataDTO> load(String domain) {
        return webApiService.loadMetadataByDomains(Sets.newHashSet(domain));
    }
}
