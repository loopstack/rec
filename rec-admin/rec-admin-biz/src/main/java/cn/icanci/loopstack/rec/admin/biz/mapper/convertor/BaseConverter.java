package cn.icanci.loopstack.rec.admin.biz.mapper.convertor;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:00
 */
public interface BaseConverter<T extends Enum<T>> {
    /**
     * 枚举转编码
     *
     * @param t 枚举
     * @return 编码
     */
    String asString(T t);

    /**
     * 编码转枚举
     *
     * @param str 编码
     * @return 枚举
     */
    public T asEnum(String str);
}
