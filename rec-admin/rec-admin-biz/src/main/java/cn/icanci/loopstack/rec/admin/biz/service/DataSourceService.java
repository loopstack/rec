package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.biz.model.DataSourceDebugResult;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.DataSourceVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 09:03
 */
public interface DataSourceService extends BaseService<DataSourceVO> {
    @Override
    List<DataSourceVO> queryAll();

    @Override
    void save(DataSourceVO dataSourceVO);

    @Override
    DataSourceVO queryById(String id);

    @Override
    PageList<DataSourceVO> queryPage(DataSourceVO dataSourceVO, int pageNum, int pageSize);

    /**
     * 根据 dataSourceName 查询数据源
     *
     * @param domainCode domainCode
     * @param dataSourceName dataSourceName
     * @return 返回 根据 dataSourceName 查询数据源
     */
    DataSourceVO queryByDataSourceName(String domainCode, String dataSourceName);

    /**
     * 下拉框加载
     * 
     * @param domainCode domainCode
     * @return 返回加载的数据
     */
    List<TextValue> loadSelector(String domainCode);

    /**
     * 对数据源进行预先执行
     * 
     * @param dataSource dataSource
     * @return 返回执行结果
     */
    DataSourceDebugResult debug(DataSourceVO dataSource);

    /**
     * 根据uuid查询
     *
     * @param dataSourceUuid dataSourceUuid
     * @return 返回查询结果
     */
    DataSourceVO queryByUuid(String dataSourceUuid);
}
