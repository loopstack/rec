package cn.icanci.loopstack.rec.admin.biz.mapper.convertor;

import cn.icanci.loopstack.rec.common.enums.DataSourceTypeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 16:59
 */
@Component
public class DataSourceTypeEnumConverter extends AbstractBaseConverter<DataSourceTypeEnum> {
}
