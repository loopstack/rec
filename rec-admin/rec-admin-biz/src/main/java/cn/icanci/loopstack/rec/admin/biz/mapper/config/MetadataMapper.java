package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.MetadataDO;
import cn.icanci.loopstack.rec.common.model.config.MetadataVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:40
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface MetadataMapper extends BaseMapper<MetadataDO, MetadataVO> {
    MetadataVO.MetadataPair do2vo(MetadataDO.MetadataPair metadataPair);

    MetadataDO.MetadataPair vo2do(MetadataVO.MetadataPair metadataPair);
}
