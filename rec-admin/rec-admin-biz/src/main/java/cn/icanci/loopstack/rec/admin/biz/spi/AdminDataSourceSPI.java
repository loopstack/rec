package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.DataSourceDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.DataSourceSPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminDataSourceSPI extends AbstractLoadSPI implements DataSourceSPI {
    @Override
    public List<DataSourceDTO> load(Set<String> domains) {
        return webApiService.loadDataSourceByDomains(domains);
    }

    @Override
    public List<DataSourceDTO> load(String domain) {
        return webApiService.loadDataSourceByDomains(Sets.newHashSet(domain));
    }
}
