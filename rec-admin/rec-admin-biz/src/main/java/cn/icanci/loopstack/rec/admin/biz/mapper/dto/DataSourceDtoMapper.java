package cn.icanci.loopstack.rec.admin.biz.mapper.dto;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.DataSourceTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.HttpRequestTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.ScriptTypeEnumConverter;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.DataSourceDO;
import cn.icanci.loopstack.rec.common.aggregation.model.DataSourceDTO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 08:19
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, ScriptTypeEnumConverter.class,
                                            HttpRequestTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DataSourceDtoMapper extends BaseDtoMapper<DataSourceDO, DataSourceDTO> {

    DataSourceDTO.ScriptInfo do2dto(DataSourceDO.ScriptInfo scriptInfo);

    DataSourceDTO.HttpInfo do2dto(DataSourceDO.HttpInfo httpInfo);

    DataSourceDTO.SqlInfo do2dto(DataSourceDO.SqlInfo sqlInfo);

}
