package cn.icanci.loopstack.rec.admin.biz.service;

import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.common.model.TextValue;
import cn.icanci.loopstack.rec.common.model.config.MetadataVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:17
 */
public interface MetadataService extends BaseService<MetadataVO> {
    @Override
    List<MetadataVO> queryAll();

    @Override
    void save(MetadataVO metadataVO);

    @Override
    MetadataVO queryById(String id);

    @Override
    PageList<MetadataVO> queryPage(MetadataVO metadataVO, int pageNum, int pageSize);

    /**
     * 加载元数据
     * 
     * @param domainCode domainCode
     * @return 返回元数据
     */
    List<TextValue> loadSelector(String domainCode);

    /**
     * 根据metadataName查询元数据
     * 
     *
     * @param metadataName metadataName
     * @param domainCode domainCode
     * @return 返回元数据
     */
    MetadataVO queryByMetadataName(String metadataName, String domainCode);
}
