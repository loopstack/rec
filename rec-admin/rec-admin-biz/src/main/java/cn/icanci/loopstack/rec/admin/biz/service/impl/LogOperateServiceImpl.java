package cn.icanci.loopstack.rec.admin.biz.service.impl;

import cn.icanci.loopstack.rec.admin.biz.mapper.config.LogOperateMapper;
import cn.icanci.loopstack.rec.admin.biz.service.LogOperateService;
import cn.icanci.loopstack.rec.admin.dal.mongodb.common.PageList;
import cn.icanci.loopstack.rec.admin.dal.mongodb.daointerface.LogOperateDAO;
import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.LogOperateDO;
import cn.icanci.loopstack.rec.common.model.log.LogOperateVO;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 10:41
 */
@Service
public class LogOperateServiceImpl implements LogOperateService {
    @Resource
    private LogOperateDAO    logOperateDAO;
    @Resource
    private LogOperateMapper logOperateMapper;

    @Override
    public void log(LogOperateVO logOperate) {
        logOperateDAO.insert(logOperateMapper.vo2do(logOperate));
    }

    @Override
    public PageList<LogOperateVO> queryPage(String module, String targetId, int pageNum, int pageSize) {
        LogOperateDO operate = new LogOperateDO();
        operate.setModule(module);
        operate.setTargetId(targetId);

        PageList<LogOperateDO> pageQuery = logOperateDAO.pageQuery(operate, pageNum, pageSize);
        return new PageList<>(logOperateMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }
}
