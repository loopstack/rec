package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.dal.mongodb.dateobject.SceneDO;
import cn.icanci.loopstack.rec.common.model.config.SceneVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:43
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface SceneMapper extends BaseMapper<SceneDO, SceneVO> {

    SceneVO.ScenePair do2vo(SceneDO.ScenePair scenePair);

    SceneDO.ScenePair vo2do(SceneVO.ScenePair scenePair);
}
