package cn.icanci.loopstack.rec.admin.biz.spi;

import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.engine.sdk.spi.StrategySPI;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/18 19:37
 */
public class AdminStrategySPI extends AbstractLoadSPI implements StrategySPI {
    @Override
    public List<StrategyDTO> load(Set<String> domains) {
        return webApiService.loadStrategyByDomains(domains);
    }

    @Override
    public List<StrategyDTO> load(String domain) {
        return webApiService.loadStrategyByDomains(Sets.newHashSet(domain));
    }

}
