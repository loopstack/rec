package cn.icanci.loopstack.rec.admin.biz.mapper.config;

import cn.icanci.loopstack.rec.admin.biz.mapper.convertor.*;
import cn.icanci.loopstack.rec.common.aggregation.model.StrategyDTO;
import cn.icanci.loopstack.rec.common.model.config.StrategyVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:49
 */
@Mapper(componentModel = "spring", uses = { DataSourceTypeEnumConverter.class, RuleTypeEnumConverter.class, OperatorEnumConverter.class, InterruptEnumConverter.class,
                                            ResultTypeEnumConverter.class, RuleModeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface StrategyVoDtoMapper {

    StrategyDTO vo2dto(StrategyVO strategyVO);

    StrategyDTO.RuleListInfo vo2dto(StrategyVO.RuleListInfo ruleListInfo);

    StrategyDTO.RuleTreeInfo vo2dto(StrategyVO.RuleTreeInfo ruleTreeInfo);

    StrategyDTO.Condition vo2dto(StrategyVO.Condition condition);

    StrategyDTO.ConditionCell vo2dto(StrategyVO.ConditionCell conditionCell);

}
