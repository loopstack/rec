package cn.icanci.loopstack.rec.admin.biz.test;
import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.rec.common.model.config.DomainVO;

import java.util.Date;

import org.junit.Test;

/**
 * @author icanci
 * @since 1.0 Created in 2022/12/01 08:50
 */
public class HutoolTest {
    @Test
    public void testJsonUtil(){
        DomainVO domainVO = new DomainVO();
        domainVO.setDomainName("");
        domainVO.setDomainCode("");
        domainVO.setId("");
        domainVO.setUuid("");
        domainVO.setDesc("");
        domainVO.setCreateTime(new Date());
        domainVO.setUpdateTime(new Date());
        domainVO.setIsDelete(0);
        domainVO.setEnv("");
        String json = JSONUtil.toJsonStr(domainVO);
        System.out.println(json);
        DomainVO domainVO1 = JSONUtil.toBean(json, DomainVO.class);
        System.out.println(domainVO1);
    }
}
